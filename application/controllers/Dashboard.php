<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if (!$this->session->userdata('userid'))
        {
            redirect(base_url());
        }
    }
    
	public function index($jenisakun,$idakun)
	{
        $dataprop=array();
        foreach ($this->db->where('k_user',$idakun)->get('plotting_user')->result_array() as $p)
        {
            $dataprop[]=(int)$p['k_wilayah'];
        }    


        $this->db->select('a.*,b.terakhir,b.sudah_jawab');
        $this->db->from('pengaduan_judul a');
        $this->db->join('pengaduan_status b','a.id=b.id','left');
        if (strtolower($jenisakun)=="operator")
        {
            if (count($dataprop)>0)
            {
                $this->db->where_in('asal_provinsi',$dataprop);
            }
            else if ($idakun!="2")
            {
                $this->db->where('asal_provinsi','111111');
            }
        }
        $query=$this->db->get();
        
        $stat1=0;
        $stat2=0;
        $stat3=0;
        $stat4=0;

        foreach ($query->result_array() as $stat)
        {
            if ($stat['status']=='1' AND $stat['sudah_jawab']=='0')
            {
                $stat1++;
            }
            else if ($stat['status']=='1' AND $stat['sudah_jawab']>0 AND $stat['terakhir']=='1')
            {
                $stat2++;
            }
            else if ($stat['status']=='1' AND $stat['sudah_jawab']>0 AND $stat['terakhir']=='0')
            {
                $stat3++;
            }
            else if ($stat['status']=='0')
            {
                $stat4++;
            }
        }

        $this->db->select('a.asal_provinsi, count(*) as jum_0,SUM(CASE WHEN a.status="1" AND b.sudah_jawab="0" THEN 1 ELSE 0 END) as jum_1, SUM(CASE WHEN a.status="1" AND b.sudah_jawab > 0 AND b.terakhir="1" THEN 1 ELSE 0 END) as jum_2, SUM(CASE WHEN a.status="1" AND b.sudah_jawab > 0 AND b.terakhir="0" THEN 1 ELSE 0 END) as jum_3, SUM(CASE WHEN a.status="0" THEN 1 ELSE 0 END) as jum_4');
        $this->db->from('pengaduan_judul a');
        $this->db->join('pengaduan_status b','a.id=b.id','left');
        if (strtolower($jenisakun)=="operator")
        {
            if (count($dataprop)>0)
            {
                $this->db->where_in('asal_provinsi',$dataprop);
            }
            else if ($idakun!="2")
            {
                $this->db->where('asal_provinsi','111111');
            }
        }
        $this->db->group_by('a.asal_provinsi');
        $this->db->order_by('count(*)','desc');
        $query=$this->db->get();
        // echo $this->db->last_query();

        $data=array(
            'jenisakun'=>$jenisakun,
            'idakun'=>$idakun,
            'stat1'=>$stat1,
            'stat2'=>$stat2,
            'stat3'=>$stat3,
            'stat4'=>$stat4
        );

        $data['queryasli']=$query;
		$this->load->view('dashboard',$data);
	}

    public function dinas($jenisakun,$idakun)
	{
        $dataprop=array();
        foreach ($this->db->where('k_user',$idakun)->get('plotting_user_dinas')->result_array() as $p)
        {
            $dataprop[]=(int)$p['k_wilayah'];
        }    


        $this->db->select('a.*,b.terakhir,b.sudah_jawab');
        $this->db->from('pengaduan_judul_dinas a');
        $this->db->join('pengaduan_status_dinas b','a.id=b.id','left');
        if (strtolower($jenisakun)=="operator")
        {
            if (count($dataprop)>0)
            {
                $this->db->where_in('asal_provinsi',$dataprop);
            }
            else if ($idakun!="2")
            {
                $this->db->where('asal_provinsi','111111');
            }
        }
        $query=$this->db->get();
        
        $stat1=0;
        $stat2=0;
        $stat3=0;
        $stat4=0;

        foreach ($query->result_array() as $stat)
        {
            if ($stat['status']=='1' AND $stat['sudah_jawab']=='0')
            {
                $stat1++;
            }
            else if ($stat['status']=='1' AND $stat['sudah_jawab']>0 AND $stat['terakhir']=='1')
            {
                $stat2++;
            }
            else if ($stat['status']=='1' AND $stat['sudah_jawab']>0 AND $stat['terakhir']=='0')
            {
                $stat3++;
            }
            else if ($stat['status']=='0')
            {
                $stat4++;
            }
        }

        $this->db->select('a.asal_provinsi, count(*) as jum_0,SUM(CASE WHEN a.status="1" AND b.sudah_jawab="0" THEN 1 ELSE 0 END) as jum_1, SUM(CASE WHEN a.status="1" AND b.sudah_jawab > 0 AND b.terakhir="1" THEN 1 ELSE 0 END) as jum_2, SUM(CASE WHEN a.status="1" AND b.sudah_jawab > 0 AND b.terakhir="0" THEN 1 ELSE 0 END) as jum_3, SUM(CASE WHEN a.status="0" THEN 1 ELSE 0 END) as jum_4');
        $this->db->from('pengaduan_judul_dinas a');
        $this->db->join('pengaduan_status_dinas b','a.id=b.id','left');
        if (strtolower($jenisakun)=="operator")
        {
            if (count($dataprop)>0)
            {
                $this->db->where_in('asal_provinsi',$dataprop);
            }
            else if ($idakun!="2")
            {
                $this->db->where('asal_provinsi','111111');
            }
        }
        $this->db->group_by('a.asal_provinsi');
        $this->db->order_by('count(*)','desc');
        $query=$this->db->get();
        // echo $this->db->last_query();

        $data=array(
            'jenisakun'=>$jenisakun,
            'idakun'=>$idakun,
            'stat1'=>$stat1,
            'stat2'=>$stat2,
            'stat3'=>$stat3,
            'stat4'=>$stat4
        );

        $data['queryasli']=$query;
		$this->load->view('dashboard_dinas',$data);
	}
}
