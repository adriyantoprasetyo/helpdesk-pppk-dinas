<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$password=$this->input->post('password',true);
		$logout=$this->input->post('logout',true);
		if (@$logout)
		{
			$this->session->unset_userdata('loginaksespppk2021');
			redirect(base_url('prodi'));
		}
		else if ($password=="pppk2021..!!" || $this->session->userdata('loginaksespppk2021')=='1')
		{
			$this->session->set_userdata('loginaksespppk2021','1');
			$this->pppk=$this->load->database('pppk',TRUE);
			$dataquery=$this->pppk->query("select * from eformasi_p3k.dbo.fn_prodi_baru('%') order by prodi")->result_array();
			echo '
			<html>
				<head>
					<title>PPPK Guru 2021</title>
					<link rel="shortcut icon" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">
					<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
					<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
						<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
						<meta http-equiv="refresh" content="30">
				</head>
				<body>
					<center><h1>Data Program Studi Baru<br/><form method="POST" action=""><input type="submit" name="logout" style="font-size:18px" value="Keluar"></form></h1></center>
					<hr>
					<table class="display" style="width:100%; font-size:16px" id="pppk">
						<thead>
							<th>No</th>
							<th>Nama Prodi</th>
						</thead>
						<tbody>';
						$no=0;
						foreach ($dataquery as $d)
						{
							$no++;
							echo "<tr>
									<td>".$no."</td>
									<td>".$d['prodi']."</td>
								  </tr>";
						}
						echo '</tbody>
					</table>
				</body>
				<script type="text/javascript">
					$(document).ready(function() {
					    $("#pppk").DataTable({
					    	iDisplayLength:-1
					    });
					});
				</script>
			';
		}
		else
		{
			echo '
			<html>
				<head>
					<title>PPPK Guru 2021</title>
					<link rel="shortcut icon" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">
				</head>
				<body>
					<h1>Masukkan Kode Akses..!!</h1>
					<hr>
					<form method="POST" action="">
					Password : <input type="password" name="password" required> <input type="submit" name="submit"> 
					</form>
				</body>
			';
		}
	}
}
