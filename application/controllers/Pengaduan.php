<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if (!$this->session->userdata('userid'))
        {
            redirect(base_url());
        }
    }

	public function index($jenisakun,$idakun)
	{
        $dataprop=array();
        foreach ($this->db->where('k_user',$idakun)->get('plotting_user_dinas')->result_array() as $p)
        {
            $dataprop[]=(int)$p['k_wilayah'];
        }    


        $this->db->select('a.*,b.terakhir,b.sudah_jawab');
        $this->db->from('pengaduan_judul_dinas a');
        $this->db->join('pengaduan_status_dinas b','a.id=b.id','left');
        if (strtolower($jenisakun)=="operator")
        {
            if (count($dataprop)>0)
            {
                $this->db->where_in('asal_provinsi',$dataprop);
            }
            else if ($idakun!="2")
            {
                $this->db->where('asal_provinsi','111111');
            }
        }
        $query=$this->db->get();
        
        $stat1=0;
        $stat2=0;
        $stat3=0;
        $stat4=0;

        foreach ($query->result_array() as $stat)
        {
            if ($stat['status']=='1' AND $stat['sudah_jawab']=='0')
            {
                $stat1++;
            }
            else if ($stat['status']=='1' AND $stat['sudah_jawab']>0 AND $stat['terakhir']=='1')
            {
                $stat2++;
            }
            else if ($stat['status']=='1' AND $stat['sudah_jawab']>0 AND $stat['terakhir']=='0')
            {
                $stat3++;
            }
            else if ($stat['status']=='0')
            {
                $stat4++;
            }
        }

        $data=array(
            'jenisakun'=>$jenisakun,
            'idakun'=>$idakun,
            'stat1'=>$stat1,
            'stat2'=>$stat2,
            'stat3'=>$stat3,
            'stat4'=>$stat4
        );
		$this->load->view('pengaduan',$data);
	}

    public function data($jenisakun,$idakun)
    {
        $dtprop=$this->input->get('prop',TRUE);
        $dtstatuss=$this->input->get('statuss',TRUE);
        $dturut=$this->input->get('urut',TRUE);

        if (!$dtprop OR $dtprop=="")
        {
            $dtprop="all";
        }

        if (!$dtstatuss OR $dtstatuss=="")
        {
            $dtstatuss="all";
        }

        if (!$dturut OR $dturut=="")
        {
            $dturut="all";
        }

        $this->load->model('Pengaduan_model','pengaduan',TRUE);
        $list = $this->pengaduan->get_datatables($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut);
        $data = array();
        $i=0;
        $no = $_POST['start'];
        if (count($list) > 0)
        {
            foreach ($list as $row) {
                $no++;
                $data=array();
                if ($row['status']=='1')
                {
                    if ($row['sudah_jawab']=='0')
                    {
                        $stts='<td class="text-center"><button class="btn btn-sm btn-danger">Belum Terjawab</button></td>';
                    }
                    else if ($row['terakhir']=='1'){
                        $stts='<td class="text-center"><button class="btn btn-sm btn-primary">Aktif - Sudah Terjawab</button></td>';
                    }
                    else{
                        $stts='<td class="text-center"><button class="btn btn-sm btn-warning">Aktif - Belum Terjawab</button></td>';
                    }
                }
                else
                {
                    $stts='<td class="text-center"><button class="btn btn-sm btn-success">Selesai</button></td>';
                }

                if (strlen($row['asal_provinsi'])=='5')
                {
                    $propi="0".$row['asal_provinsi'];
                }
                else
                {
                    $propi=$row['asal_provinsi'];
                }
                $propinsi=$this->db->where('kode_wilayah',$propi)->get('master_propinsi')->row_array()['nama'];
                
                if ($row['jenis']=='dinas')
                {
                    $colo="blue";
                }
                else
                {
                    $colo="red";
                }

                $data[]=$no;
                $data[]=$row['tiket']."<br><font color='".$colo."' style='font-size:12px'>(".$row['jenis'].")</font>";
                $data[]=$row['nama']."<br><font color='blue' style='font-size:12px'>(".$propinsi.")</font>";
                $data[]=$row['judul'];
                $data[]=datetime_to_id($row['created_at']);
                $data[]=$stts;
                $data[]='<a href="'.base_url('index.php/pengaduan/detail/'.strToHex($row['id'])).'"
                class="btn btn-sm btn-primary"><i class="dripicons-message"></i>
                Balas</a>';
                $hasil[]=$data;
            }
        }
        else{
            $hasil[]=array('','','','','','','');
        }
        $total=$this->pengaduan->count_all($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut);
        $filtered=$this->pengaduan->count_filtered($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut);
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $total,
                "recordsFiltered" => $filtered,
                "data" => $hasil,
        );
        //output to json format
        echo json_encode($output);
    }

    public function detail($idpengaduan)
    {
        $id=hexToStr($idpengaduan);
        $data['pengaduan']=$this->db->where('id',$id)->get('pengaduan_judul_dinas')->row_array();
        $data['riwayat']=$this->db->where('pengaduan_judul_id',$id)->where('deleted_at IS NULL',null)->get('pengaduan_riwayat_dinas')->result_array();
        $this->load->view('pengaduan_detail',$data);
    }

    public function send($tiket)
    {
        $pengaduan=$this->db->where('tiket',$tiket)->get('pengaduan_judul_dinas')->row_array();
        $temp=array(
            'pengaduan_judul_id'=>$pengaduan['id'],
            'isi'=>$this->input->post('isi',true),
            'who'=>'1',
            'user_id'=>$this->session->userdata('userid'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        );
        $u=$this->db->insert('pengaduan_riwayat_dinas',$temp);
        if ($u)
        {
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'http://10.10.10.199/layanan/email/pppk_helpdesk_dinas',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => 'idpesan='.$pengaduan['id'].'&jenis=balas&key=96acd8e3496766d4b0f004c4be8670f6',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $output['status']=true;
            $output['message']="Data Berhasil disimpan";
        }
        else
        {
            $output['status']=false;
            $output['message']=$this->db->last_query();
        }
        echo json_encode($output);
    }

    public function ubah_chat($tiket)
    {
        $pengaduan=$this->db->where('id',$tiket)->get('pengaduan_riwayat_dinas')->row_array();
        $output['message']=$pengaduan['isi'];
        echo json_encode($output);
    }

    public function close_chat($tiket)
    {
        $jenis=$this->input->post('_token',TRUE);
        if (@$jenis)
        {
            $id=$this->db->where('tiket',$tiket)->get('pengaduan_judul_dinas')->row_array()['id'];

            $temp=array(
                'status'=>'0',
                'tutup_at'=>date("Y-m-d H:i:s"),
                'tutup_by'=>$this->session->userdata('userid')
            );
            $u=$this->db->where('tiket',$tiket)->update('pengaduan_judul_dinas',$temp);
            if ($u)
            {
                $this->session->set_flashdata('msg','Data Pengaduan dengan Tiket '.$tiket.' berhasil ditutup');
                redirect(base_url('index.php/pengaduan/detail/'.strToHex($id).''));
            }
        }
    }

    public function update_chat($tiket)
    {
        $id=$this->input->post('id',true);
        $isi=$this->input->post('isi',true);

        $cek=$this->db->where('id',$id)->get('pengaduan_riwayat_dinas');

        if ($cek->num_rows() > 0)
        {
            $temp=array(
                'isi'=>$isi,
                'updated_at'=>date("Y-m-d H:i:s")
            );
            $this->db->where('id',$id)->update('pengaduan_riwayat_dinas',$temp);

            $output['status']=true;
        }
        else
        {
            $output['status']=false;
        }
        echo json_encode($output);
    }

    public function delete_chat($tiket)
    {
        $id=$this->input->post('id',true);

        $cek=$this->db->where('id',$id)->get('pengaduan_riwayat_dinas');
        $r=$cek->row_array();

        if ($cek->num_rows() > 0)
        {
            $temp=array(
                'deleted_at'=>date("Y-m-d H:i:s")
            );
            $this->db->where('id',$id)->update('pengaduan_riwayat_dinas',$temp);

            $output['status']=true;
        }
        else
        {
            $output['status']=false;
        }
        echo json_encode($output);
    }
}
