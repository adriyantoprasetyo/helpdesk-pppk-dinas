<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plotting extends CI_Controller {

	public function __construct(){
        parent::__construct();
        if (!$this->session->userdata('userid'))
        {
            redirect(base_url());
        }
    }
    
	public function index()
	{
        $this->plot();
	}

	public function plot()
	{
		$this->load->view('plotting');
	}

	protected function get_wilayah_arr($propinsi)
	{
		$get = $this->db->select('GROUP_CONCAT(k_user) as k_user')->where(array('k_wilayah'=>$propinsi))->get('plotting_user_dinas')->row();
		if($get->k_user!=NULL){
			$explode = explode(',', $get->k_user);
		} else{
			$explode = [];
		}
		
		return $explode;
	}

	public function save()
	{
		$propinsi = $this->input->post('instansis', true);
		$new = $this->input->post('propinsis', true);

		$jenis=$this->input->post('jenis',true);

		$exis = $this->get_wilayah_arr($propinsi);
		
		//array_diff($instansi_existed, $instansi);
		$del = [];

		if (count($new)==0)
		{
			$this->db->where(array('k_wilayah'=>$propinsi))->delete('plotting_user_dinas');
		}
		else
		{
			$arr_to_delete = array_diff($exis, $new);
			//$count_to_delete = count($arr_to_delete);
			for($i=0; $i<count($exis); $i++){
				if(in_array($exis[$i], $arr_to_delete)){
					$this->db->where(array('k_wilayah'=>$propinsi, 'k_user'=>$arr_to_delete[$i]))->delete('plotting_user_dinas');
				}
			}

			//array_diff($instansi, $instansi_existed);
			$ins = [];
			$arr_to_insert = array_diff($new, $exis);
			//$count_to_insert = count($arr_to_insert);
			for($i=0; $i<count($new); $i++){
				if(in_array($new[$i], $arr_to_insert)){
					$this->db->insert('plotting_user_dinas', array('k_wilayah'=>$propinsi, 'k_user'=>$arr_to_insert[$i], 'created'=>date("Y-m-d H:i:s")));
				}
			}
		}
		//if ($this->All_crud->update_response('petugas', $id, $data_petugas)==true) {
			//$data['message'] = '<font color="#44b6ae"><i class="fa fa-check"></i> Berhasil Diupdate.</font>';
			$data['message'] = 'Data disimpan.';
			//$data['redirect_url'] = site_url('back/admin/bangunan');
			$response = [
				'status'=>true,
				'data'=>$data
			];
		/*} else {
			$data['message'] = '<font color="#e73d4a"><i class="fa fa-times"></i> Gagal Diupdate. System Error</font>';
			$response = [
				'status'=>false,
				'data'=>$data
			];
		}*/
		//}
	// }
	
	echo json_encode($response);
	}
}