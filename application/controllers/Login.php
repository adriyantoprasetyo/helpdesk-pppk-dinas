<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
        if ($this->session->userdata('role')!="")
        {
            redirect(base_url('index.php/dashboard/dinas/'.$this->session->userdata('jenisakun').'/1'));
        }
        else
        {
		  $this->load->view('login');
	    }
    }

    public function aksi()
    {
        $post=$this->input->post();
        if (@$post)
        {
            $username=$this->input->post('username',TRUE);
            $password=$this->input->post('password',TRUE);

            $cek=$this->db->where('name',$username)->get('users');
            if ($cek->num_rows() > 0)
            {
                $r=$cek->row_array();
                if ($r['password2']==md5(md5($password)))
                {
                    $temp=array(
                        'username'=>$username,
                        'email'=>$r['email'],
                        'userid'=>$r['id'],
                        'role'=>$r['role_id'],
                        'jenisakun'=>$r['jenisakun']
                    );
                    $this->session->set_userdata($temp);
                    redirect(base_url('index.php/dashboard/dinas/'.$r['jenisakun'].'/1'));
                }
                else
                {
                    $this->session->set_flashdata('msg','Password tidak sesuai !');
                    redirect(base_url());
                }
            }
            else
            {
                $this->session->set_flashdata('msg','Username tidak terdaftar !');
                redirect(base_url());
            }
        }
        else
        {
            $this->session->set_flashdata('msg','Silakan coba kembali !');
            redirect(base_url());
        }
    }

    public function logout()
    {
        session_destroy();
        redirect(base_url());
    }
}
