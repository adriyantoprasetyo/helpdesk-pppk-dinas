<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan_model extends CI_Model {

    var $table = 'pengaduan_judul_dinas a';
    var $column_order = array('tiket','nama','judul'); //set column field database for datatable orderable
    var $column_search = array('tiket','nama','judul'); //set column field database for datatable searchable
    var $order = array('created_at' => 'desc'); // default order


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut)
    {
        if (strtolower($jenisakun)=="operator")
        {
            // if ($idakun!="2")
            // {
                $dataprop=array();
                foreach ($this->db->where('k_user',$idakun)->get('plotting_user_dinas')->result_array() as $p)
                {
                    $dataprop[]=(int)$p['k_wilayah'];
                }    
            // }
        }

        $this->db->select('a.*,b.terakhir,b.sudah_jawab');
        $this->db->from($this->table);
        $this->db->join('pengaduan_status_dinas b',$this->table.'.id=b.id','left');
        
        if ($dtprop!=NULL AND $dtprop!="all")
        {
            $this->db->where('asal_provinsi',$dtprop);
        }
        else if (strtolower($jenisakun)=="operator")
        {
            if (count($dataprop)>0)
            {
                $this->db->where_in('asal_provinsi',$dataprop);
            }
            else if ($idakun!="2")
            {
                $this->db->where('asal_provinsi','111111');
            }
        }

        if ($dtstatuss!=NULL AND $dtstatuss!="all")
        {
            if ($dtstatuss=="1")
            {
                $this->db->group_start();
                $this->db->where('a.status','1');
                $this->db->where('b.sudah_jawab','0');
                $this->db->group_end();
            }
            else if ($dtstatuss=="2")
            {
                $this->db->group_start();
                $this->db->where('a.status','1');
                $this->db->where('b.sudah_jawab >','0');
                $this->db->where('b.terakhir','1');
                $this->db->group_end();
            }
            else if ($dtstatuss=="3")
            {
                $this->db->group_start();
                $this->db->where('a.status','1');
                $this->db->where('b.sudah_jawab >','0');
                $this->db->where('b.terakhir','0');
                $this->db->group_end();
            }
            else if ($dtstatuss=="4")
            {
                $this->db->where('a.status','0');
            }
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket

                // $this->cache->memcached->delete('countfiltered'.$this->session->userdata('lembaga'));
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if ($dturut!="all")
        {
            if ($dturut=="lama")
            {
                $this->db->order_by('created_at','desc');
            }
            else if ($dturut=="baru")
            {
                $this->db->order_by('created_at','asc');
            }
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut)
    {
        $this->_get_datatables_query($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut);
        if($_POST['length'] != -1)
        $this->db->limit(10, $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
        //$this->output->enable_profiler(TRUE);
    }

    function count_filtered($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut)
    {
        $this->_get_datatables_query($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($jenisakun,$idakun,$dtprop,$dtstatuss,$dturut)
    {
        if (strtolower($jenisakun)=="operator")
        {
            $dataprop=array();
            foreach ($this->db->where('k_user',$idakun)->get('plotting_user_dinas')->result_array() as $p)
            {
                $dataprop[]=(int)$p['k_wilayah'];
            }
        }

        $this->db->select('a.*');
        $this->db->from($this->table);
        $this->db->join('pengaduan_status_dinas b',$this->table.'.id=b.id','left');
        if ($dtprop!=NULL AND $dtprop!="all")
        {
            $this->db->where('asal_provinsi',$dtprop);
        }
        else if (strtolower($jenisakun)=="operator")
        {
            if (count($dataprop)>0)
            {
                $this->db->where_in('asal_provinsi',$dataprop);
            }
            else
            {
                $this->db->where('asal_provinsi','111111');
            }
        }

        if ($dtstatuss!=NULL AND $dtstatuss!="all")
        {
            if ($dtstatuss=="1")
            {
                $this->db->group_start();
                $this->db->where('a.status','1');
                $this->db->where('b.sudah_jawab','0');
                $this->db->group_end();
            }
            else if ($dtstatuss=="2")
            {
                $this->db->group_start();
                $this->db->where('a.status','1');
                $this->db->where('b.sudah_jawab >','0');
                $this->db->where('b.terakhir','1');
                $this->db->group_end();
            }
            else if ($dtstatuss=="3")
            {
                $this->db->group_start();
                $this->db->where('a.status','1');
                $this->db->where('b.sudah_jawab >','0');
                $this->db->where('b.terakhir','0');
                $this->db->group_end();
            }
            else if ($dtstatuss=="4")
            {
                $this->db->where('a.status','0');
            }
        }

        $tot=$this->db->count_all_results();
        // $this->cache->memcached->save('countall'.$this->session->userdata('lembaga'), $tot, 60);
        return $tot;
    }

}
