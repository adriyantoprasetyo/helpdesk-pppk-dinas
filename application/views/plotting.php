<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>PPPK Guru Kemdikbudristek</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Seleksi Penerimaan PPPK Guru Kemdikbudristek Tahun 2021" name="description" />
    <meta content="Ditjen GTK Kemdikbudristek" name="author" />
    <link rel="shortcut icon" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">

    <!-- Bootstrap Css -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/assets/libs/sweetalert2/sweetalert2.min.css">
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet"
    type="text/css" />
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet"
    type="text/css" />

</head>


<body data-sidebar="dark">

    <!-- <body data-layout="horizontal" data-topbar="dark"> -->

    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- BEGIN: Header-->
        <header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="#" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png" alt="" height="17">
                    </span>
                </a>

                <a href="#" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png" alt="" height="60"
                            width="200">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <!-- App Search-->
            <form class="app-search d-none d-lg-block">
                <div class="position-relative">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="bx bx-search-alt"></span>
                </div>
            </form>

        </div>

        <div class="d-flex">

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user"
                        src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png" alt="Header Avatar">
                    <span class="d-none d-xl-inline-block ml-1" key="t-henry">admin</span>
                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <!-- <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i> <span key="t-profile">Profile</span></a> -->
                    <a class="dropdown-item text-danger" href="<?php echo base_url('index.php/login/logout')?>"><i
                            class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> <span
                            key="t-logout">Logout</span></a>
                </div>
            </div>

        </div>
    </div>
</header>
        <!-- END: Header-->


        <!-- BEGIN: Sidebar-->
        <!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                                    <li class="menu-title" key="t-menu">Menu</li>
                    <li>
                        <a href="<?php echo base_url('index.php/dashboard/dinas/'.$this->session->userdata('jenisakun').'/'.$this->session->userdata('userid').'')?>">
                            <i class="bx bx-home-circle"></i>
                            
                            <span key="t-dashboards">Dashboard</span>
                        </a>
                    </li>

                    <li class="menu-title" key="t-informasi">Informasi</li>
                    
                    <?php
                    if ($this->session->userdata('jenisakun')=="admin")
                    {
                        ?>
                        <li>
                            <a href="<?php echo base_url('index.php/plotting')?>" class="waves-effect">
                                <i class="bx bx-stats"></i>
                                <span key="t-chat">Plotting</span>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                    <li>
                        <a href="<?php echo base_url('index.php/pengaduan/index/'.$this->session->userdata('jenisakun').'/'.$this->session->userdata('userid').'')?>" class="waves-effect">
                            <i class="bx bx-chat"></i>
                            <span key="t-chat">Pengaduan</span>
                        </a>
                    </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End
        <!-- END: Sidebar-->


        <!-- BEGIN: Content-->
            <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Plotting PIC Pengaduan</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Informasi</a></li>
                                    <li class="breadcrumb-item active">Plotting PIC</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Daftar Wilayah Pengaduan</h4>
                                <br />
                                <table class="table table-bordered dt-responsive nowrap"
                                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:3%">No</th>
                                            <th class="text-center" style="width:25%">Nama Propinsi</th>
                                            <th class="text-center">Nama Operator</th>
                                        </tr>
                                    </thead>
                                    <tbody id="wilayah_pengaduan">
                                        <?php
                                        $no=0;
                                        foreach ($this->db->order_by('nama','asc')->get('master_propinsi')->result_array() as $p)
                                        {
                                            $no++;
                                            ?>
                                        <tr>
                                            <td class="text-center"><?php echo $no ?></td>
                                            <td><?php echo $p['nama']?></td>
                                            <td class="text-center">
                                                <select name="pic" class="select2 form-control select2-multiple" <?php echo 'onChange="getSelectedOptions2(\''.$p['kode_wilayah'].'\',this)"'?> multiple="multiple">
                                                    <?php
                                                    $arraypil=array();
                                                    foreach ($this->db->where('k_wilayah',$p['kode_wilayah'])->get('plotting_user_dinas')->result_array() as $v)
                                                    {
                                                        $arraypil[]=$v['k_user'];
                                                    }
                                                    foreach ($this->db->where('role_id','1')->where('password2 IS NOT NULL',null)->get('users')->result_array() as $pic)
                                                    {
                                                        $select="";
                                                        if (count($arraypil)>0)
                                                        {
                                                            if (in_array($pic['id'],$arraypil))
                                                            {
                                                                $select="selected";
                                                            }
                                                        }
                                                        echo "<option value='".$pic['id']."' ".$select.">".$pic['fullname']."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>    
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                     </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- END: Content-->

    </div>
    <!-- END layout-wrapper -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- JAVASCRIPT -->
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/jquery/jquery.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/metismenu/metisMenu.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/simplebar/simplebar.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/node-waves/waves.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- App js -->
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/js/app.js"></script>

    <!-- BEGIN: Page JS-->
        <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/sweetalert2/sweetalert2.min.js"></script>
    <script>
        function show_swal(tipe, title, message) {
            Swal.fire(
                title, message, tipe
            )
        }
    </script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/select2/js/select2.min.js"></script>
    <script>
        let default_lang = {
            paginate: {
                next: 'Selanjutnya',
                previous: 'Sebelumnya'
            },
            lengthMenu: "Menampilkan _MENU_ baris",
            search: 'Cari:',
            info: 'Menampilkan _START_ ke _END_ dari _TOTAL_ baris',
            infoEmpty: 'Kosong',
            emptyTable: 'Tidak ada data yang dapat ditampilkan',
            infoFiltered: '(tersaring dari _MAX_ baris)'
        };
    </script>
    <script>
        $('#datatable').DataTable();
        $(".select2").select2();

        function getSelectedOptions2(instansi, propinsi) {
		    var opts = [], opt;
		    var len = propinsi.options.length;
		    
		    for (var i = 0; i < len; i++) {
		        opt = propinsi.options[i];
		        if (opt.selected) {
		            opts.push(opt.value);
		        }
		    }
		    
            $.ajax({
                url: '<?php echo base_url('index.php/plotting/save')?>',
                type: 'POST',
                data: {propinsis: opts, instansis: instansi},
                dataType: "JSON",
                success: function(response){
                    if(response.status==true){
                        Swal.fire({
                            title: 'Berhasil!',
                            text: 'Data Berhasil Disimpan',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 1000
                        });
                    } else{
                        Swal.fire({
                            title: 'Gagal!',
                            text: 'Data Gagal Disimpan',
                            icon: 'danger',
                            showConfirmButton: false,
                            timer: 1000
                        });
                        if(typeof(response.data.redirect_url)!=='undefined' && response.data.redirect_url!==null){
                            setTimeout(function(){
                                window.location.href = response.data.redirect_url;
                            }, 2000);
                        }
                    }

                },
                error: function(){
                    console.log('error');
                    /*$('#processing').html('<font color="#e73d4a">Server error. Try again.</font>');
                    $('#btn-update').prop('disabled', false);*/
                }
            });
        }
    </script>
    <!-- END: Page JS-->

</body>

</html>
