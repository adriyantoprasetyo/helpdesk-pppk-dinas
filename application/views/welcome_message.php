
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="l0lLBdgOdCjQiBy8YFhCL8hNbjJsUM3wxHcmDIqN">

    <title>PPPK Guru Kemdikbudristek</title>
    <meta name="author" content="Dirjen GTK Kemdikbudristek">
    <meta name="description" content="PPPK Kemdikbudristek">
    <!-- favicon icon -->
    <link rel="shortcut icon" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">
    <link rel="apple-touch-icon" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">
    <!-- style sheets and font icons  -->
    <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/frontend/css/font-icons.min.css">
    <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/frontend/css/theme-vendors.min.css">
    <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/frontend/css/style.css" />
    <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/frontend/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/frontend/css/simplebar.min.css">

    

    <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/frontend/css/custom.css">
    <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/frontend/css/custom-2.css">
    <link href="https://gurupppk.kemdikbud.go.id/public/frontend/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://gurupppk.kemdikbud.go.id/public/frontend/css/print.min.css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-Q8BDM2EQW6"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-Q8BDM2EQW6');
    </script>
    <style>
    .txt-judul-info{
    color: #3d99fd40
}

.black-move .swiper-slide, .black-move .swiper-wrapper{
    display: flex;
    align-items: center;
}

.text-overlap-style-02.custnyaa{
    left: 15px;
    top: -25px;
    text-shadow: 0 0px 30px #0000000a;
}

.cust-cardd{
    border-radius: 20px;
    border: none;
    box-shadow: 0 0 30px 0px #0000000a;
}

.overlap-section-cust-infoo{
    position: relative;
    margin-top: -30px;
}
</style>
</head>

<body data-mobile-nav-style="classic">
    <!-- start header -->
    <header>
        <!-- start navigation -->
        <nav
            class="navbar navbar-expand-lg navbar-dark bg-transparent header-light fixed-top navbar-boxed header-always-fixed-scroll">
            <div class="container-fluid nav-header-container">
                <div class="col-auto col-sm-6 col-lg-2 mr-auto pl-lg-0">
                    <a class="navbar-brand" href="https://gurupppk.kemdikbud.go.id">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png"
                            data-at2x="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png" class="default-logo"
                            alt="">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png"
                            data-at2x="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png" class="alt-logo" alt="">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png"
                            data-at2x="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png" class="mobile-logo"
                            alt="">
                    </a>
                </div>
                <div class="col-auto col-lg-8 menu-order px-lg-0">
                    <button class="navbar-toggler float-right" type="button" data-toggle="collapse"
                        data-target="#navbarNav" aria-controls="navbarNav" aria-label="Toggle navigation">
                        <span class="navbar-toggler-line"></span>
                        <span class="navbar-toggler-line"></span>
                        <span class="navbar-toggler-line"></span>
                        <span class="navbar-toggler-line"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                        <ul class="navbar-nav alt-font">
                            <li class="nav-item">
                                <a href="#tentang" class="nav-link section-link">Beranda</a>
                            </li>
                            <li class="nav-item">
                                <a href="#syarat" class="nav-link section-link">Alur Seleksi</a>
                            </li>
                            <li class="nav-item">
                                <a href="#unduhan" class="nav-link section-link">Unduh Dokumen</a>
                            </li>
                            <li class="nav-item">
                                <a href="#faq" class="nav-link section-link">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a href="#kontak" class="nav-link section-link">Kontak</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-auto col-lg-2 text-right pr-0 font-size-0">
                    <!-- class="ml-auto" <div class="header-search-icon search-form-wrapper">
                            <a href="javascript:void(0)" class="search-form-icon header-search-form"><i class="feather icon-feather-search"></i></a>
                            <div class="form-wrapper">
                                <button title="Close" type="button" class="search-close alt-font">×</button>
                                <form id="search-form" role="search" method="get" class="search-form text-left" action="search-result.html">
                                    <div class="search-form-box">
                                        <span class="search-label alt-font text-small text-uppercase text-medium-gray">What are you looking for?</span>
                                        <input class="search-input alt-font" id="search-form-input5e219ef164995" placeholder="Enter your keywords..." name="s" value="" type="text" autocomplete="off">
                                        <button type="submit" class="search-button">
                                            <i class="feather icon-feather-search" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div> -->
                    
                                    </div>
            </div>
        </nav>

        

            </header>
    <!-- end header -->

    
    <main>
            <section class="parallax p-0 home-digital-agency" data-parallax-background-ratio="0.5"
        style="background-image: url('public/frontend/images/custom/bg-1.jpg')">
        <div class="overlay-bg bg-extra-dark-gray" style="opacity: 0.6;"></div>
        <!-- <div class="opacity-light bg-gradient-dark-slate-blue-transparent"></div> -->
        <div class="container">
            <div class="row">
                <div class="col-12 full-screen md-h-600px sm-h-350px d-flex flex-column">
                    <br/>
                    <h1
                        class="alt-font font-weight-600 text-white title-large w-50 mt-auto mb-0 letter-spacing-minus-5px sm-letter-spacing-minus-1-half xs-w-70 tit-utama">
                        PPPK Guru</h1>
                    <div class="tp-caption mt-4 ml-md-1 tp-resizeme alt-font text-extra-large font-weight-500 d-inline-block text-uppercase mt-2"
                        data-x="['left','left','left','center']" data-hoffset="['2','42','35','0']"
                        data-y="['middle','middle','middle','bottom']" data-voffset="['-130','-140','-110','340']"
                        data-width="none" data-height="none" data-whitespace="nowrap" data-fontsize="['15','15','15','15']"
                        data-lineheight="['15','15','15','15']" data-color="#BF8C4C" data-type="text"
                        data-responsive_offset="on" data-responsive="on"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":500,"frame":"0","from":"sX:0.8;sY:0.8;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","split":"chars","splitdelay":0.03,"speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;","ease":"Power3.easeIn"}]'
                        data-textAlign="['left','left','center','center']" style="color: rgba(255, 255, 255, 0.5);">
                        Rekrutmen Guru PPPK</div>


                    <div class="">
                        <?php
                        $h=0;
                        if ($h==1)
                        {
                            ?>
                    <a class="col-auto mr-auto section-link tp-caption btn btn-extra-large btn-expand-ltr collection-btn btn-rounded text-extra-dark-gray font-weight-600 d-md-block d-none"
                        href="https://sscasn.bkn.go.id" target="_blank" data-x="['left','left','left','center']"
                        data-hoffset="['0','40','34','0']" data-y="['middle','middle','middle','bottom']"
                        data-voffset="['150','95','79','100']" data-whitespace="nowrap"
                        data-fontsize="['15','12','12','12']" data-line-height="['15','12','12','12']" data-type="button"
                        data-responsive="off" data-responsive_offset="on"
                        data-frames='[{"delay":1100,"speed":200,"frame":"0","from":"o:0;","to":"o:1;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['left','left','left','center']" style="width:fit-content; color: white;">Jadwal Seleksi &nbsp;<p class="badge badge-warning" style="margin-bottom:0px">New</p>
                        <i class="line-icon-Arrow-OutRight right-icon" aria-hidden="true"
                            style="opacity: 1; font-size: 20px; line-height: 30px; margin-left: 17px; transition: 0.3s ease-in-out;"></i>
                        <span
                            style="background: #3d99fd; opacity: 0.3; width: 40px; height: 40px; transform: translateY(-50%); top: 50%;"></span> </a>
                        <?php
                        }
                        ?>
                    <a class="col-auto mr-auto section-link tp-caption btn btn-extra-large btn-expand-ltr collection-btn btn-rounded text-extra-dark-gray font-weight-600 d-md-block d-none"
                        href="https://sscasn.bkn.go.id" target="_blank" data-x="['left','left','left','center']"
                        data-hoffset="['0','40','34','0']" data-y="['middle','middle','middle','bottom']"
                        data-voffset="['150','95','79','100']" data-whitespace="nowrap"
                        data-fontsize="['15','12','12','12']" data-line-height="['15','12','12','12']" data-type="button"
                        data-responsive="off" data-responsive_offset="on"
                        data-frames='[{"delay":1100,"speed":200,"frame":"0","from":"o:0;","to":"o:1;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['left','left','left','center']" style="width:fit-content; color: white;">Daftar Seleksi PPPK Guru
                        <i class="line-icon-Arrow-OutRight right-icon" aria-hidden="true"
                            style="opacity: 1; font-size: 30px; line-height: 30px; margin-left: 17px; transition: 0.3s ease-in-out;"></i>
                        <span
                            style="background: #3d99fd; opacity: 0.3; width: 40px; height: 40px; transform: translateY(-50%); top: 50%;"></span></a>

                    <a class="col-auto mr-auto section-link tp-caption btn btn-extra-large btn-expand-ltr collection-btn btn-rounded text-extra-dark-gray font-weight-600 d-md-block d-none"
                        href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/Pengumuman_4040BGT01002021.pdf" target="_blank" data-x="['left','left','left','center']"
                        data-hoffset="['0','40','34','0']" data-y="['middle','middle','middle','bottom']"
                        data-voffset="['150','95','79','100']" data-whitespace="nowrap"
                        data-fontsize="['15','12','12','12']" data-line-height="['15','12','12','12']" data-type="button"
                        data-responsive="off" data-responsive_offset="on"
                        data-frames='[{"delay":1100,"speed":200,"frame":"0","from":"o:0;","to":"o:1;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['left','left','left','center']" style="width:fit-content; color: white;">Perubahan Pengumuman Seleksi PPPK Guru
                        <i class="line-icon-Arrow-OutRight right-icon" aria-hidden="true"
                            style="opacity: 1; font-size: 30px; line-height: 30px; margin-left: 17px; transition: 0.3s ease-in-out;"></i>
                        <span
                            style="background: #3d99fd; opacity: 0.3; width: 40px; height: 40px; transform: translateY(-50%); top: 50%;"></span></a>

                    <a class="col-auto mr-auto section-link tp-caption btn btn-extra-large btn-expand-ltr collection-btn btn-rounded text-extra-dark-gray font-weight-600 d-md-block d-none"
                        href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/210701_Pengumuman_Seleksi_PPPK_Guru_1.pdf" target="_blank" data-x="['left','left','left','center']"
                        data-hoffset="['0','40','34','0']" data-y="['middle','middle','middle','bottom']"
                        data-voffset="['150','95','79','100']" data-whitespace="nowrap"
                        data-fontsize="['15','12','12','12']" data-line-height="['15','12','12','12']" data-type="button"
                        data-responsive="off" data-responsive_offset="on"
                        data-frames='[{"delay":1100,"speed":200,"frame":"0","from":"o:0;","to":"o:1;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['left','left','left','center']" style="width:fit-content; color: white;">Pengumuman Seleksi PPPK Guru
                        <i class="line-icon-Arrow-OutRight right-icon" aria-hidden="true"
                            style="opacity: 1; font-size: 30px; line-height: 30px; margin-left: 17px; transition: 0.3s ease-in-out;"></i>
                        <span
                            style="background: #3d99fd; opacity: 0.3; width: 40px; height: 40px; transform: translateY(-50%); top: 50%;"></span></a>
                    </div>
                    <!-- <div class="d-flex flex-row align-items-center mt-auto margin-7-half-rem-bottom"> -->
                    <div class="d-flex flex-row align-items-center mt-auto mb-md-5 mb-3">
                        <span class="bg-white w-50 h-1px margin-9-rem-right sm-w-40 sm-margin-5-rem-right xs-w-15 xs-margin-2-rem-right"></span>
                        <span class="alt-font text-large text-white">Portal Informasi Pendaftaran Guru PPPK</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="overflow-visible py-0 px-md-0 px-3" style="background-image: url('public/home-startup-about-bg.jpg');background-attachment: fixed;">
        <div class="card container py-5 cust-cardd overlap-section-cust-infoo">
            <div class="row justify-content-center">
                <!-- <div class="col-12 text-center">
                    <span class="alt-font text-medium text-olivine-green text-uppercase font-weight-500 d-block margin-15px-bottom sm-margin-10px-bottom">INFORMASI</span>
                    <h5 class="alt-font text-dark-charcoal font-weight-700 text-uppercase letter-spacing-minus-1px margin-40px-bottom w-80 mx-auto xs-w-100">Need a consultant for your business?</h5>
                </div> -->
                <div class="col-12 text-center margin-1-rem-bottom">
                    <div class="d-flex flex-row align-items-center justify-content-center text-center margin-5px-bottom">
                        <span class="w-25px h-1px bg-green opacity-4"></span>
                        <div class="alt-font text-extra-medium text-green padding-10px-lr">PENGUMUMAN</div>
                        <span class="w-25px h-1px bg-green opacity-4"></span>
                    </div>
                </div>
                <div class="col-12 col-lg-11 padding-5-rem-lr sm-padding-5-rem-lr xs-padding-5-rem-lr wow animate__fadeIn">
                    <div class="swiper-container slider-custom-text black-move" data-slider-options='{ "loop": true, "autoHeight": "on", "keyboard": { "enabled": true, "onlyInViewport": true }, "autoplay": { "delay": 4000, "disableOnInteraction": false }, "navigation": { "nextEl": ".swiper-button-next-nav", "prevEl": ".swiper-button-previous-nav" }, "effect": "slide" }'>
                        <div class="swiper-wrapper lh-12">
                            <div class="swiper-slide">
                                <div class="d-flex flex-column">
                                    <div class="align-self-center text-center w-100">
                                        <p class="text-black text-large line-height-36px md-line-height-30px w-90 d-inline-block mb-0">
                                            <b>Pengumuman Nomor : 5001/B/GT.01.00/2021 <br/> Ketentuan Pelaksanaan Seleksi Kompetensi I Guru ASN-PPPK Tahun 2021</b>
                                        </p>
                                        <img alt="" src="https://gurupppk.kemdikbud.go.id/public/upload/ketentuan_pelaksanaan_tes.PNG" class="fluid" style="text-align: center;margin-top:15px;width:80%">
                                        <hr>
                                        <p class="text-black text-large line-height-30px md-line-height-30px w-90 d-inline-block mb-0" style="text-align: justify;">
                                            Mohon pelamar agar selalu memantau perkembangan informasi pada laman <a href="https://gurupppk.kemdikbud.go.id" style="color:blue" target="_blank">https://gurupppk.kemdikbud.go.id</a>.<br/>&nbsp;
                                        </p>
                                        <br/><br/>
                                        <center><a target="_blank" href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/Pengumuman_Ketentuan_Pelaksanaan_Tes_Kompetensi_I_Seleksi_P3K.pdf" class="btn btn-block btn-rounded btn-primary">Unduh Dokumen Pengumuman</a></center>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="d-flex flex-column">
                                    <div class="align-self-center text-center w-100">
                                        <p class="text-black text-large line-height-36px md-line-height-30px w-90 d-inline-block mb-0">
                                            <b>Pengumuman Nomor : 4661/B/GT.01.00/2021 <br/> Jadwal tahapan pelaksanaan Seleksi Guru ASN-PPPK Tahun 2021</b>
                                        </p>
                                        <img alt="" src="https://gurupppk.kemdikbud.go.id/public/upload/penyampaian_jadwal_tahapan_seleksi_guru_pppk_2021.PNG" class="fluid" style="text-align: center;margin-top:15px;width:80%">
                                        <hr>
                                        <p class="text-black text-large line-height-30px md-line-height-30px w-90 d-inline-block mb-0" style="text-align: justify;">
                                            Mohon pelamar agar selalu memantau perkembangan informasi pada laman <a href="https://gurupppk.kemdikbud.go.id" style="color:blue" target="_blank">https://gurupppk.kemdikbud.go.id</a>.<br/>&nbsp;
                                        </p>
                                        <br/><br/>
                                        <center><a target="_blank" href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/penyampaian_jadwal_tahapan_seleksi_guru_pppk_2021.pdf" class="btn btn-block btn-rounded btn-primary">Unduh Dokumen Pengumuman</a></center>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="d-flex flex-column">
                                    <div class="align-self-center text-center w-100">
                                        <p class="text-black text-large line-height-36px md-line-height-30px w-90 d-inline-block mb-0">
                                            <b>Penyesuaian kembali jadwal tahapan Pelaksanaan Seleksi PPPK Guru Tahun 2021</b>
                                        </p>
                                        <img alt="" src="https://gurupppk.kemdikbud.go.id/public/upload/penyesuaian_jadwal.PNG" class="fluid" style="text-align: center;margin-top:15px;width:80%">
                                        <hr>
                                        <p class="text-black text-large line-height-30px md-line-height-30px w-90 d-inline-block mb-0" style="text-align: justify;">
                                            Hasil Seleksi Administrasi dapat dilihat pada akun SSCASN masing-masing pelamar mulai tanggal 10-08-2021 pukul 18.00 WIB. <br/><br/>Apabila ada pertanyaan silahkan menghubungi <a href="https://gurupppk.kemdikbud.go.id/contact" style="color:blue" target="_blank">https://gurupppk.kemdikbud.go.id/contact</a>.
                                        </p>
                                        <br/><br/>
                                        <center><a target="_blank" href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/Penyesuaian_kembali_jadwal_tahapan_Pelaksanaan_Seleksi_PPPK_Guru.pdf" class="btn btn-block btn-rounded btn-primary">Unduh Dokumen Pengumuman</a></center>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="d-flex flex-column">
                                    <div class="align-self-center w-100" style="text-align: left">
                                        <img alt="" src="images/home-yoga-meditation-icon-quote.jpg" class="w-70px margin-50px-bottom xs-w-50px xs-margin-25px-bottom" data-no-retina="">
                                        <p class="text-large d-inline-block">
                                            Menindaklanjuti Surat Edaran Kementerian Pendayagunaan Aparatur Negara Dan Reformasi Birokrasi Republik Indonesia Nomor B/1075/S.SM.01.00/2021 Tentang Ketentuan Peserta Seleksi Kompetensi I pada Pengadaan PPPK untuk JF Guru Tahun 2021, dan dengan mempertimbangkan Dapodik Kemendikbudristek terkait penugasan guru pada Satuan Administrasi Pangkal serta jenis jabatannya, dengan ini disampaikan Pengumuman sebagai berikut:</p>
                                        <ol>
                                        	<li style="margin-top:7px;margin-bottom: 7px">Terdapat penyesuaian aplikasi pendaftaran seleksi Guru ASN-PPPK Tahun 2021 terkait penentuan individu peserta yang dapat mengikuti seleksi kompetensi pertama, berupa tombol reset pendaftaran, yang ditujukan bagi:
                                        		<ul>
                                        			<li style="margin-left: 20px">Guru dengan penugasan di sekolah induk yang terdapat formasi dan seharusnya dapat melamar ke formasi tersebut, namun formasinya tidak dapat dipilih dikarenakan kuota formasi yang terkunci, sehingga terpaksa melamar ke formasi di sekolah lain;</li>
                                        			<li style="margin-left: 20px">Guru dengan penugasan di sekolah induk yang tidak terdapat formasi dan seharusnya dapat melamar ke sekolah lain yang masih memiliki sisa kuota formasi, namun formasinya tidak dapat dipilih dikarenakan kuota formasi yang terkunci; dan</li>
                                        			<li style="margin-left: 20px">Guru dengan penugasan di sekolah induk yang tidak terdapat formasi dan sudah mendaftar ke sekolah lain yang tidak memiliki sisa kuota formasi, dikarenakan prioritas bagi guru yang bertugas di sekolah lain tersebut.</li>
                                        		</ul>
                                        	</li>
                                        	<li style="margin-top:7px;margin-bottom: 7px">Fasilitas penyesuaian pada aplikasi pendaftaran tersebut hanya dapat dipergunakan satu kali. Mohon dipergunakan dengan seksama/teliti untuk dapat menyesuaikan lamaran ke formasi sekolah induk/tempat bertugas atau sekolah lain dalam wilayah instansi kewenangan yang sama. Setelah mempergunakan fasilitas tersebut, harap dapat dipastikan kembali kondisi final resume sebelum batas akhir waktu pendaftaran.</li>
                                        	<li style="margin-top:7px;margin-bottom: 7px">Bagi Guru yang sudah menetapkan pilihan formasi dan tidak terkendala oleh formasi yang terkunci baik di sekolah induk/tempat tugas maupun di sekolah lain dalam satu wilayah instansi kewenangan yang sama, fasilitas penyesuaian dalam aplikasi pendaftaran seleksi Guru ASN-PPPK Tahun 2021 dapat diabaikan.</li>
                                        </ol>
                                        <p class="text-large d-inline-block">Penjelasan lebih lanjut dapat menghubungi layanan bantuan seleksi Guru ASN-PPPK pada alamat <a style="color:blue" href="https://gurupppk.kemdikbud.go.id/contact" target="_blank">https://gurupppk.kemdikbud.go.id/contact</a>.<br/><br/>Demikian pengumuman ini disampaikan untuk diketahui.</p>
                                        <br/><br/>
                                        <center><a target="_blank" href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/Pengumuman_Penyesuaian_Aplikasi_SSCASN.pdf" class="btn btn-block btn-rounded btn-primary">Unduh Dokumen Pengumuman</a></center>
                                        <!-- <span class="alt-font font-weight-500 text-green text-uppercase d-block">Alexander harvard</span>
                                        <span class="alt-font text-small text-uppercase d-block">Meditation expert</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="d-flex flex-column">
                                    <div class="align-self-center text-center w-100">
                                        <img alt="" src="images/home-yoga-meditation-icon-quote.jpg" class="w-70px margin-50px-bottom xs-w-50px xs-margin-25px-bottom">
                                        <p class="text-black text-large line-height-36px md-line-height-30px w-90 d-inline-block mb-0">
                                            <b>Data yang digunakan pada pendaftaran SSCASN PPPK Guru adalah data periode Desember 2020. Pemutakhiran data yang dilakukan setelah periode tersebut tidak dijamin terekam pada seleksi pendaftaran Guru ASN-PPPK di aplikasi SSCASN</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- start slider pagination -->
                    <div class="swiper-button-next-nav slider-custom-text-next swiper-button-next font-weight-600 alt-font text-small text-dark-purple"><i class="fa fa-chevron-right"></i></div>
                    <div class="swiper-button-previous-nav slider-custom-text-prev swiper-button-prev font-weight-600 alt-font text-small text-dark-purple"><i class="fa fa-chevron-left"></i></div>
                    <!-- end slider pagination -->
            </div>
        </div>
    </section>
    <!-- <section class="overflow-visible cover-background wow animate__fadeIn" style="background-image:url('images/home-decor-bg-img-02.jpg');"> -->
    <section id="tentang" class="overflow-visible wow animate__fadeIn">
        <div class="row d-md-none d-block">
                <div class="col-12 overlap-section md-no-margin-top md-margin-70px-bottom sm-margin-50px-bottom">
                    <div class="row row-cols-1 row-cols-lg-3 row-cols-md-2 justify-content-center">
                        <?php
                        $h=0;
                        if ($h==1)
                        {
                            ?>
                        <div class="col col-sm-8 md-margin-30px-bottom xs-margin-15px-bottom">
                            <!-- start feature box item-->
                            <div class="feature-box h-100 feature-box-left-icon-middle padding-1-half-rem-all bg-white box-shadow-small box-shadow-large-hover border-radius-4px overflow-hidden last-paragraph-no-margin lg-padding-2-half-rem-tb lg-padding-2-rem-lr md-padding-2-rem-all">
                                <div class="feature-box-icon margin-20px-right lg-margin-15px-right">
                                    <i class="line-icon-Cursor-Click2 icon-medium text-green"></i>
                                </div>
                                <div class="feature-box-content">
                                    <div class="text-slate-blue font-weight-500 text-large margin-5px-bottom lh-11">Jadwal Seleksi <span class="badge badge-warning">New</span></div>
                                    <a href="https://sscasn.bkn.go.id/" class="btn btn-link btn-medium text-extra-dark-gray md-margin-10px-bottom">Lihat selengkapnya</a>
                                </div>
                            </div>
                            <!-- end feature box item-->
                        </div>
                        <?php
                        }
                        ?>
                        <div class="col col-sm-8 md-margin-30px-bottom xs-margin-15px-bottom">
                            <!-- start feature box item-->
                            <div class="feature-box h-100 feature-box-left-icon-middle padding-1-half-rem-all bg-white box-shadow-small box-shadow-large-hover border-radius-4px overflow-hidden last-paragraph-no-margin lg-padding-2-half-rem-tb lg-padding-2-rem-lr md-padding-2-rem-all">
                                <div class="feature-box-icon margin-20px-right lg-margin-15px-right">
                                    <i class="line-icon-Cursor-Click2 icon-medium text-green"></i>
                                </div>
                                <div class="feature-box-content">
                                    <div class="text-slate-blue font-weight-500 text-large margin-5px-bottom lh-11">Daftar Seleksi PPPK Guru</div>
                                    <a href="https://sscasn.bkn.go.id/" class="btn btn-link btn-medium text-extra-dark-gray md-margin-10px-bottom">Lihat selengkapnya</a>
                                </div>
                            </div>
                            <!-- end feature box item-->
                        </div>
                        <div class="col col-sm-8 md-margin-30px-bottom xs-margin-15px-bottom">
                            <!-- start feature box item-->
                            <div class="feature-box h-100 feature-box-left-icon-middle padding-1-half-rem-all bg-white box-shadow-small box-shadow-large-hover border-radius-4px overflow-hidden last-paragraph-no-margin lg-padding-2-half-rem-tb lg-padding-2-rem-lr md-padding-2-rem-all">
                                <div class="feature-box-icon margin-20px-right lg-margin-15px-right">
                                    <i class="line-icon-Folder-WithDocument icon-medium text-green"></i>
                                </div>
                                <div class="feature-box-content">
                                    <div class="text-slate-blue font-weight-500 text-large margin-5px-bottom lh-11">Perubahan Pengumuman Seleksi PPPK Guru</div>
                                    <a href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/Pengumuman_4040BGT01002021.pdf" class="btn btn-link btn-medium text-extra-dark-gray md-margin-10px-bottom">Lihat selengkapnya</a>
                                </div>
                            </div>
                            <!-- end feature box item-->
                        </div>
                        <div class="col col-sm-8">
                            <!-- start feature box item-->
                            <div class="feature-box h-100 feature-box-left-icon-middle padding-1-half-rem-all bg-white box-shadow-small box-shadow-large-hover border-radius-4px overflow-hidden last-paragraph-no-margin lg-padding-2-half-rem-tb lg-padding-2-rem-lr md-padding-2-rem-all">
                                <div class="feature-box-icon margin-20px-right lg-margin-15px-right">
                                    <i class="line-icon-Boy icon-medium text-green"></i>
                                </div>
                                <div class="feature-box-content">
                                    <div class="text-slate-blue font-weight-500 text-large margin-5px-bottom lh-11">Pengumuman Seleksi PPPK Guru</div>
                                    <a href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/210701_Pengumuman_Seleksi_PPPK_Guru_1.pdf" class="btn btn-link btn-medium text-extra-dark-gray md-margin-10px-bottom">Lihat selengkapnya</a>
                                </div>
                            </div>
                            <!-- end feature box item-->
                        </div>
                    </div>
                </div>
            </div>
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-lg-6 col-md-10 margin-20px-bottom md-margin-8-rem-bottom sm-margin-10-rem-bottom wow animate__fadeIn"
                    data-wow-delay="0.2s">
                    <div class="img-fluid">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/guru_pppk.png" alt="" data-no-retina="">
                    </div>
                </div>
                <div class="col-12 col-lg-5 offset-lg-1 col-md-10 wow animate__fadeIn" data-wow-delay="0.5s">
                    <div class="alt-font text-medium font-weight-500 margin-30px-bottom media"><span
                            class="w-40px h-1px bg-tussock opacity-7 align-self-center margin-20px-right"></span>
                        <div class="media-body"><span class="text-tussock text-uppercase">Tentang PPPK Guru</span></div>
                    </div>
                    <h4
                        class="alt-font text-extra-dark-gray font-weight-600 w-85 margin-35px-bottom lg-w-100 sm-margin-25px-bottom">
                        Apa itu PPPK ?</h4>
                    <p class="w-80 lg-w-100">Pegawai Pemerintah dengan Perjanjian Kerja yang selanjutnya disingkat PPPK
                        adalah warga negara Indonesia yang memenuhi syarat tertentu, yang diangkat berdasarkan perjanjian
                        kerja untuk jangka waktu tertentu dalam rangka melaksanakan tugas pemerintahan.</p>
                </div>
            </div>
        </div>
    </section>
    
        <section class="big-section bg-light-gray wow animate__fadeIn pb-0" id="syarat">
        <div class="container">
            <div class="row justify-content-center">
                <!-- <div class="col-md-12 text-center margin-seven-bottom">
                                        <h6 class="alt-font text-extra-dark-gray font-weight-500">Pengumuman</h6>
                                    </div> -->
                <div class="col-12 text-center margin-4-rem-bottom">
                    <span class="d-block alt-font margin-5px-bottom text-uppercase">Informasi</span>
                    <h5 class="alt-font text-extra-dark-gray font-weight-600 mb-0">Alur Seleksi</h5>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 mx-au hide-mobile" style="margin-bottom: 50px">
                    <img class="fluid" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/alur_seleksi.png" style="margin-left:-80px">
                </div>
                <div class="col-12 col-md-8 mx-au hide-pc" style="margin-bottom: 50px">
                    <img class="fluid" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/alur_seleksi.png">
                </div>
                <div class="hide-mobile">
                    <div class="containers" style="height:190px;width:215px;margin-left:-600px;margin-top:185px;border:0px solid #000">
                      <div class="imgs">
                        <a onclick="location.href='#verval';return false;" class="preview" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/verval_ijazah.png"><img class="img" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/kemdikbud-xs.png" style="height:100%;opacity: 0"></a>
                      </div>
                    </div>
                    <div class="containers" style="height:60px;width:100px;margin-left:-365px;margin-top:-116px;border:0px solid #000">
                      <div class="imgs">
                        <a onclick="location.href='#formasi1';return false;" class="preview" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/formasi.png"><img class="img" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/kemdikbud-xs.png" style="height:100%;opacity: 0"></a>
                      </div>
                    </div>
                    <div class="containers" style="height:60px;width:120px;margin-left:-210px;margin-top:-46px;border:0px solid #000">
                      <div class="imgs">
                        <a onclick="location.href='#formasi3';return false;" class="preview" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/formasi.png"><img class="img" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/kemdikbud-xs.png" style="height:100%;width:100%;opacity: 0"></a>
                      </div>
                    </div>
                    <div class="containers" style="height:60px;width:120px;margin-left:-210px;margin-top:-240px;border:0px solid #000">
                      <div class="imgs">
                        <a onclick="location.href='#formasi2';return false;" class="preview" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/formasi.png"><img class="img" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/kemdikbud-xs.png" style="height:100%;width:100%;opacity: 0"></a>
                      </div>
                    </div>
                    <div class="containers" style="height:65px;width:190px;margin-left:-240px;margin-top:-140px;border:0px solid #000">
                      <div class="imgs">
                        <a onclick="location.href='#uk1';return false;" class="previewright" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/uk1.png"><img class="img" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/kemdikbud-xs.png" style="height:100%;width:100%;opacity: 0"></a>
                      </div>
                    </div>
                    <div class="containers" style="height:65px;width:190px;margin-left:-240px;margin-top:100px;border:0px solid #000">
                      <div class="imgs">
                        <a onclick="location.href='#uk2';return false;" class="previewright" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/uk2.png"><img class="img" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/kemdikbud-xs.png" style="height:100%;width:100%;opacity: 0"></a>
                      </div>
                    </div>
                    <div class="containers" style="height:65px;width:190px;margin-left:-240px;margin-top:110px;border:0px solid #000">
                      <div class="imgs">
                        <a onclick="location.href='#uk3';return false;" class="previewright" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/uk3.png"><img class="img" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/kemdikbud-xs.png" style="height:100%;width:100%;opacity: 0"></a>
                      </div>
                    </div>
                    <div class="containers" style="height:170px;width:155px;margin-left:-770px;margin-top:-210px;border:0px solid #000">
                      <div class="imgs">
                        <a onclick="location.href='#pendaftaran';return false;"><img class="img" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/pendaftaran.png" style="height:100%;cursor: pointer;"></a>
                      </div>
                    </div>
                </div>
                <div id="pendaftaran" style="margin-top:30px"></div>
                <div class="col-12 col-md-12 mx-au" style="margin-top:10px;padding-top:20px;border-top:1px solid #ccc">
                    <ul class="list-style-06">
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="verval">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/pendaftaran.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Pendaftaran Akun<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b>- 01 s.d 22 Juli 2021</b></span></span>
                                <p class="w-100" style="text-align: justify;">Pelamar melakukan pendaftaran akun melalui Laman <a href="https://sscasn.bkn.go.id" target="_blank">https://sscasn.bkn.go.id</a>.</p>
                            </div>
                        </li>
                        <hr>
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="formasi1">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/verval.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Verval Ijazah<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b></b></span></span>
                                <p class="w-100" style="text-align: justify;">Pastikan ijazah / latar belakang pendidikan telah terverifikasi melalui INFO GTK (<a href="https://info.gtk.kemdikbud.go.id" target="_blank">https://info.gtk.kemdikbud.go.id</a>)</p>
                            </div>
                        </li>
                        <hr>
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/formasi1.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Pemilihan Formasi<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b></b></span></span>
                                <p class="w-100" style="text-align: justify;">Pelamar memilih formasi yang linier berdasarkan Data Kualifikasi Pendidikan dan/atau Sertifikat Pendidik yang telah terverifikasi. Pelamar memilih formasi dengan ketentuan sebagai berikut :
<ul style="list-style-type: square;">
<li style="padding-bottom:5px">a <span style="padding-left:15px">Dalam hal formasi tersedia di sekolah tempat pelamar mengajar saat ini, pelamar wajib mendaftar di sekolah tersebut selama sertifikat pendidik dan/atau kualifikasi pendidikan linier;</span></li>
<li style="padding-bottom:5px">b <span style="padding-left:15px">Formasi yang sudah dilamar sebagaimana dimaksud dalam huruf a, tidak dapat dilamar oleh pelamar lain; dan</span></li>
<li style="padding-bottom:5px">c <span style="padding-left:15px">Dalam hal formasi tidak tersedia di sekolah tempat pelamar mengajar, pelamar dapat mendaftar di sekolah lain yang masih tersedia formasinya.</span></li>
</ul></p>
                            </div>
                        </li>
                        <hr>
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="uk1">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/administrasi.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Seleksi Administrasi<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b>- 02 s.d 27 Juli 2021</b></span></span>
                                <p class="w-100" style="text-align: justify;">Pelamar mengunggah dokumen persyaratan yang diperlukan untuk seleksi Administrasi. <br/><a href="#" data-toggle="modal" data-target="#modal-3" class="btn btn-sm btn-primary">Info Lebih Lanjut</a></p>
                            </div>
                        </li>
                        <hr>
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="formasi2">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/uk1.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Ujian Seleksi Kompetensi<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b>- 23 s.d 29 Agustus 2021</b></span></span>
                                <p class="w-100" style="text-align: justify;">Pelamar yang lulus seleksi administrasi melaksanakan Ujian Kompetensi sesuai Formasi yang dipilih. Tahap ini hanya dapat diikuti oleh pelamar dengan kriteria sebagai berikut:
<ul style="list-style-type: square;margin-top:-5px;margin-bottom:5px">
<li style="padding-bottom:5px">a <span style="padding-left:15px">Guru non-ASN yang mengajar di Satuan Pendidikan yang diselenggarakan oleh instansi daerah dan terdaftar sebagai guru di Dapodik; dan</li>
<li style="padding-bottom:5px">b <span style="padding-left:15px">Tenaga Honorer Eks Kategori II sesuai database Tenaga Honorer Badan Kepegawaian Negara.</li>
</ul><a href="#" data-toggle="modal" data-target="#modal-soal" class="btn btn-sm btn-primary">Bobot dan durasi soal</a> <a href="#" data-toggle="modal" data-target="#modal-afirmasi" class="btn btn-sm btn-success">Kebijakan Afirmasi</a></p>
                            </div>
                        </li>
                        <hr>
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="uk2">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/formasi2.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Pemilihan Formasi II<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b>- 18 s.d 24 September 2021</b></span></span>
                                <p class="w-100" style="text-align: justify;">Bagi Pelamar yang tidak lulus Ujian Seleksi Kompetensi I dapat memilih kembali formasi dalam <font color='blue'><b>instansi kewenangan yang sama</b></font>. Seleksi kompetensi II dapat diikuti oleh:<br/>
<ul style="list-style-type: square;">
<li style="padding-bottom:5px">a <span style="padding-left:15px">Guru non-ASN yang mengajar di Satuan Pendidikan yang diselenggarakan oleh instansi daerah dan terdaftar sebagai guru di Dapodik yang tidak lulus seleksi kompetensi I;</li>
<li style="padding-bottom:5px">b <span style="padding-left:15px">Tenaga Honorer Eks Kategori II sesuai database Tenaga Honorer Badan Kepegawaian Negara yang tidak lulus seleksi kompetensi I;</li>
<li style="padding-bottom:5px">c <span style="padding-left:15px">Guru Swasta yang mengajar di Satuan Pendidikan yang diselenggarakan oleh masyarakat dan terdaftar sebagai guru di Dapodik; dan</li>
<li style="padding-bottom:5px">d <span style="padding-left:15px">Lulusan Pendidikan Profesi Guru yang belum menjadi guru dan terdaftar di database lulusan Pendidikan Profesi Guru Kemdikbudristek.</li>
</ul></p>
                            </div>
                        </li>
                        <hr>
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="formasi3">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/uk2.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Ujian Seleksi Kompetensi II<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b>- 06 s.d 12 Oktober 2021</b></span></span>
                                <p class="w-100" style="text-align: justify;">Pelamar melaksanakan Ujian Kompetensi II sesuai Formasi yang dipilih.</p>
                            </div>
                        </li>
                        <hr>
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="uk3">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/formasi3.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Pemilihan Formasi III<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b>- 29 Oktober s.d 04 November 2021</b></span></span>
                                <p class="w-100" style="text-align: justify;">Bagi Pelamar yang tidak lulus Ujian Seleksi Kompetensi II dapat memilih kembali formasi di <font color='blue'><b>seluruh wilayah Indonesia</b></font>. Seleksi kompetensi III dapat diikuti oleh:<br/>
<ul style="list-style-type: square;">
<li style="padding-bottom:5px">a <span style="padding-left:15px">Guru non-ASN yang mengajar di Satuan Pendidikan yang diselenggarakan oleh instansi daerah dan terdaftar sebagai guru di Dapodik yang tidak lulus seleksi kompetensi II;</li>
<li style="padding-bottom:5px">b <span style="padding-left:15px">Tenaga Honorer Eks Kategori II sesuai database Tenaga Honorer Badan Kepegawaian Negara yang tidak lulus seleksi kompetensi II;</li>
<li style="padding-bottom:5px">c <span style="padding-left:15px">Guru Swasta yang mengajar di Satuan Pendidikan yang diselenggarakan oleh masyarakat dan terdaftar sebagai guru di Dapodik yang tidak lulus seleksi kompetensi II; dan</li>
<li style="padding-bottom:5px">d <span style="padding-left:15px">Lulusan Pendidikan Profesi Guru yang belum menjadi guru dan terdaftar di database lulusan Pendidikan Profesi Guru Kemdikbudristek yang tidak lulus seleksi kompetensi II.</li>
</ul></p>
                            </div>
                        </li>
                        <hr>
                                                <li class="border-radius-6px margin-35px-bottom last-paragraph-no-margin wow animate__fadeIn"
                            data-wow-delay="0.2s" id="">
                            <img class="float-left img-fluid margin-20px-right hvr-bounce-in" width="100" src="https://gurupppk.kemdikbud.go.id/public/frontend/images/seleksi/uk3.svg">
                            <div><span
                                    class="text-extra-medium text-dark-charcoal font-weight-500 margin-5px-bottom d-block">Ujian Seleksi Kompetensi III<span class="font-weight-600" style="font-size: 12px;color:blue">&nbsp;&nbsp;<b>- 15 s.d 21 November 2021</b></span></span>
                                <p class="w-100" style="text-align: justify;">Pelamar melaksanakan Ujian Kompetensi III sesuai Formasi yang dipilih.</p>
                            </div>
                        </li>
                        <hr>
                                            </ul>
                </div>
            </div>
        </div>
    </section>
            <section
        class="padding-eight-bottom border-bottom border-color-extra-light-gray padding-5-rem-top md-padding-8-rem-bottom sm-padding-50px-top"
        id="unduhan">
        <div class="container">
            <div class="row justify-content-center">
                <div
                    class="col-12 col-lg-5 col-sm-7 text-center margin-3-rem-bottom md-margin-2-rem-bottom sm-margin-2-rem-bottom wow animate__fadeIn">
                    <span
                        class="alt-font font-weight-600 text-uppercase text-gradient-magenta-orange-2 d-block letter-spacing-1px margin-20px-bottom sm-margin-10px-bottom">Download
                        Dokumen</span>
                    <h5 class="alt-font font-weight-500 text-medium-slate-blue">Unduh Dokumen PPPK Guru Tahun 2021</h5>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-12 wow animate__fadeInRight" data-wow-delay="0.4s">
                    <div class="testimonials-carousel-style-02 swiper-simple-arrow-style-2">
                        <div class="swiper-container black-move"
                            data-slider-options='{ "slidesPerView": 1, "spaceBetween": 0, "observer": true, "observeParents": true, "autoplay": { "delay": 3000, "disableOnInteraction": false },  "navigation": { "nextEl": ".swiper-button-next-nav-2", "prevEl": ".swiper-button-previous-nav-2" }, "keyboard": { "enabled": true, "onlyInViewport": true }, "breakpoints": { "992": { "slidesPerView": 3 }, "768": { "slidesPerView": 2 } }, "effect": "slide" }'>
                            <div class="swiper-wrapper">
                                                                    <div class="swiper-slide text-center padding-15px-all">
                                        <div
                                            class="feature-box text-left feature-box-show-hover box-shadow-small-hover feature-box-bg-white-hover border-all border-color-black-transparent overflow-hidden">
                                            <div
                                                class="feature-box-move-bottom-top padding-3-rem-tb padding-4-rem-lr md-padding-2-rem-tb md-padding-2-half-rem-lr sm-padding-3-rem-tb sm-padding-4-half-rem-lr">
                                                <center><img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/icon-pdf.png"
                                                        style="width:30%"></center><br />
                                                <div class="feature-box-content last-paragraph-no-margin">
                                                    <span
                                                        class="text-extra-dark-gray text-extra-medium font-weight-600 text-uppercase d-block margin-10px-bottom alt-font">Perubahan Pengumuman Seleksi Penerimaan PPPK Guru Tahun 2021</span>
                                                    <p>Perubahan Pengumuman Seleksi Penerimaan PPPK Guru Tahun 2021</p>
                                                </div>
                                                <div class="move-bottom-top margin-15px-top">
                                                    <a target="_blank"
                                                        href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/Pengumuman_4040BGT01002021.pdf"
                                                        class="btn btn-link p-0 btn-extra-large text-yellow-ochre-light text-yellow-ochre-light-hover md-margin-auto-lr">Selengkapnya</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                    <div class="swiper-slide text-center padding-15px-all">
                                        <div
                                            class="feature-box text-left feature-box-show-hover box-shadow-small-hover feature-box-bg-white-hover border-all border-color-black-transparent overflow-hidden">
                                            <div
                                                class="feature-box-move-bottom-top padding-3-rem-tb padding-4-rem-lr md-padding-2-rem-tb md-padding-2-half-rem-lr sm-padding-3-rem-tb sm-padding-4-half-rem-lr">
                                                <center><img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/icon-pdf.png"
                                                        style="width:30%"></center><br />
                                                <div class="feature-box-content last-paragraph-no-margin">
                                                    <span
                                                        class="text-extra-dark-gray text-extra-medium font-weight-600 text-uppercase d-block margin-10px-bottom alt-font">Pengumuman Seleksi Penerimaan PPPK Guru Tahun 2021</span>
                                                    <p>Pengumuman Seleksi Penerimaan Pegawai Pemerintah dengan Perjanjian Kerja untuk Jabatan Fungsional Guru pada Pemerintah Daerah Tahun 2021</p>
                                                </div>
                                                <div class="move-bottom-top margin-15px-top">
                                                    <a target="_blank"
                                                        href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/210701_Pengumuman_Seleksi_PPPK_Guru_1.pdf"
                                                        class="btn btn-link p-0 btn-extra-large text-yellow-ochre-light text-yellow-ochre-light-hover md-margin-auto-lr">Selengkapnya</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                    <div class="swiper-slide text-center padding-15px-all">
                                        <div
                                            class="feature-box text-left feature-box-show-hover box-shadow-small-hover feature-box-bg-white-hover border-all border-color-black-transparent overflow-hidden">
                                            <div
                                                class="feature-box-move-bottom-top padding-3-rem-tb padding-4-rem-lr md-padding-2-rem-tb md-padding-2-half-rem-lr sm-padding-3-rem-tb sm-padding-4-half-rem-lr">
                                                <center><img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/icon-pdf.png"
                                                        style="width:30%"></center><br />
                                                <div class="feature-box-content last-paragraph-no-margin">
                                                    <span
                                                        class="text-extra-dark-gray text-extra-medium font-weight-600 text-uppercase d-block margin-10px-bottom alt-font">Juknis Pelaksanaan Seleksi PPPK Guru 2021</span>
                                                    <p>Peraturan Direktur Jenderal GTK Nomor 3767/B.B1/HK.01.03/2021 Tentang Petunjuk Teknis Pelaksanaan Seleksi Pengadaan PPPK untuk Jabatan Fungsional Guru pada Pemerintah Daerah tahun 2021</p>
                                                </div>
                                                <div class="move-bottom-top margin-15px-top">
                                                    <a target="_blank"
                                                        href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/dokumen_60eb0ab254bec1626016434.pdf"
                                                        class="btn btn-link p-0 btn-extra-large text-yellow-ochre-light text-yellow-ochre-light-hover md-margin-auto-lr">Selengkapnya</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                    <div class="swiper-slide text-center padding-15px-all">
                                        <div
                                            class="feature-box text-left feature-box-show-hover box-shadow-small-hover feature-box-bg-white-hover border-all border-color-black-transparent overflow-hidden">
                                            <div
                                                class="feature-box-move-bottom-top padding-3-rem-tb padding-4-rem-lr md-padding-2-rem-tb md-padding-2-half-rem-lr sm-padding-3-rem-tb sm-padding-4-half-rem-lr">
                                                <center><img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/icon-pdf.png"
                                                        style="width:30%"></center><br />
                                                <div class="feature-box-content last-paragraph-no-margin">
                                                    <span
                                                        class="text-extra-dark-gray text-extra-medium font-weight-600 text-uppercase d-block margin-10px-bottom alt-font">SE Linieritas Kualifikasi Akademik dan Sertifikat Pendidik</span>
                                                    <p>Surat Edaran Direktur Jenderal GTK Kemendikbud Nomor 1460/B.B1/GT.02.01/2021 Tentang Kualifikasi Akademik dan Sertifikat Pendidik dalam Pendaftaran Pengadaan Guru PPPK Tahun 2021</p>
                                                </div>
                                                <div class="move-bottom-top margin-15px-top">
                                                    <a target="_blank"
                                                        href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/SE_Kesesuaian_Kualifikasi_Akademik_dan_Sertifikat_Pendidik.pdf"
                                                        class="btn btn-link p-0 btn-extra-large text-yellow-ochre-light text-yellow-ochre-light-hover md-margin-auto-lr">Selengkapnya</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                    <div class="swiper-slide text-center padding-15px-all">
                                        <div
                                            class="feature-box text-left feature-box-show-hover box-shadow-small-hover feature-box-bg-white-hover border-all border-color-black-transparent overflow-hidden">
                                            <div
                                                class="feature-box-move-bottom-top padding-3-rem-tb padding-4-rem-lr md-padding-2-rem-tb md-padding-2-half-rem-lr sm-padding-3-rem-tb sm-padding-4-half-rem-lr">
                                                <center><img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/icon-pdf.png"
                                                        style="width:30%"></center><br />
                                                <div class="feature-box-content last-paragraph-no-margin">
                                                    <span
                                                        class="text-extra-dark-gray text-extra-medium font-weight-600 text-uppercase d-block margin-10px-bottom alt-font">SE Dirjen GTK Seleksi PPPK Guru Tahun 2021</span>
                                                    <p>Surat Edaran Direktur Jenderal GTK Kemendikbudristek tentang Seleksi PPPK Guru Tahun 2021</p>
                                                </div>
                                                <div class="move-bottom-top margin-15px-top">
                                                    <a target="_blank"
                                                        href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/210520_SE_Dirjen_Seleksi_Guru_P3K.pdf"
                                                        class="btn btn-link p-0 btn-extra-large text-yellow-ochre-light text-yellow-ochre-light-hover md-margin-auto-lr">Selengkapnya</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                    <div class="swiper-slide text-center padding-15px-all">
                                        <div
                                            class="feature-box text-left feature-box-show-hover box-shadow-small-hover feature-box-bg-white-hover border-all border-color-black-transparent overflow-hidden">
                                            <div
                                                class="feature-box-move-bottom-top padding-3-rem-tb padding-4-rem-lr md-padding-2-rem-tb md-padding-2-half-rem-lr sm-padding-3-rem-tb sm-padding-4-half-rem-lr">
                                                <center><img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/icon-pdf.png"
                                                        style="width:30%"></center><br />
                                                <div class="feature-box-content last-paragraph-no-margin">
                                                    <span
                                                        class="text-extra-dark-gray text-extra-medium font-weight-600 text-uppercase d-block margin-10px-bottom alt-font">Permen PANRB Nomor 28 Tahun 2021</span>
                                                    <p>Peraturan Menteri PANRB Nomor 28 Tahun 2021 tentang Pengadaan PPPK untuk jabatan fungsional guru pada instansi daerah tahun 2021</p>
                                                </div>
                                                <div class="move-bottom-top margin-15px-top">
                                                    <a target="_blank"
                                                        href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/Permen_PANRB_No_28_Tahun_2021_seleksi_PPPK_Guru.pdf"
                                                        class="btn btn-link p-0 btn-extra-large text-yellow-ochre-light text-yellow-ochre-light-hover md-margin-auto-lr">Selengkapnya</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-light-gray" id="faq">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center margin-7-rem-bottom">
                    <span class="d-block alt-font margin-5px-bottom text-uppercase">Informasi</span>
                    <h5 class="alt-font text-extra-dark-gray font-weight-600 mb-0">Frequently Asked Questions</h5>
                </div>
                <div class="col-12 col-md-12 md-margin-50px-bottom sm-margin-30px-bottom wow animate__fadeIn">
                    <div class="panel-group accordion-event accordion-style-03" id="accordion2"
                        data-active-icon="fa-angle-down" data-inactive-icon="fa-angle-right">
                                                                                                        <div class="panel bg-white box-shadow-small border-radius-5px mb-3">
                                <div class="panel-heading">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                        href="#collapse2" aria-expanded="false">
                                        <div class="panel-title">
                                            <span
                                                class="alt-font text-extra-dark-gray d-inline-block font-weight-500">Siapa saja yang dapat mendaftar pada Seleksi PPPK Guru 2021 ?</span>
                                            <i class="indicator fas fa-angle-down text-fast-blue icon-extra-small"></i>
                                        </div>
                                    </a>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse"
                                    data-parent="#accordion2">
                                    <div class="panel-body"><ol>
<li>Honorer THK-II sesuai database THK-II di BKN</li>
<li>Guru Honorer yang masih aktif mengajar di Sekolah Negeri di bawah kewenangan Pemerintah Daerah dan terdaftar sebagai Guru di DAPODIK Kemendikbud</li>
<li>Guru yang masih aktif mengajar di Sekolah Swasta dan terdaftar sebagai Guru di DAPODIK Kemendikbud</li>
<li>Lulusan Pendidikan Profesi Guru (PPG) yang belum menjadi guru dan terdaftar di Database Lulusan Pendidikan Profesi Guru Kemendikbud</li>
<li>Berdasarkan Surat Edaran Direktur Jenderal Guru dan Tenaga Kependidikan Kementerian Pendidikan dan Kebudayaan No 1460/B.B1/GT.02.01/2021 tanggal 15 Maret 2021</li>
</ol></div>
                                </div>
                            </div>
                                                                                <div class="panel bg-white box-shadow-small border-radius-5px mb-3">
                                <div class="panel-heading">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                        href="#collapse3" aria-expanded="false">
                                        <div class="panel-title">
                                            <span
                                                class="alt-font text-extra-dark-gray d-inline-block font-weight-500">Berapa batas usia melamar PPPK Guru?</span>
                                            <i class="indicator fas fa-angle-down text-fast-blue icon-extra-small"></i>
                                        </div>
                                    </a>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse"
                                    data-parent="#accordion2">
                                    <div class="panel-body">Usia paling rendah 20 (dua puluh) tahun dan paling tinggi 59 (lima puluh sembilan) pada saat pendaftaran</div>
                                </div>
                            </div>
                                                                                <div class="panel bg-white box-shadow-small border-radius-5px mb-3">
                                <div class="panel-heading">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                        href="#collapse4" aria-expanded="false">
                                        <div class="panel-title">
                                            <span
                                                class="alt-font text-extra-dark-gray d-inline-block font-weight-500">Bagaimana cara mendaftar PPPK Guru ?</span>
                                            <i class="indicator fas fa-angle-down text-fast-blue icon-extra-small"></i>
                                        </div>
                                    </a>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse"
                                    data-parent="#accordion2">
                                    <div class="panel-body">Pendaftaran PPPK Guru 2021 dapat dilakukan melalui website SSCASN. SSCASN atau Sistem Seleksi Calon Aparatur Sipil Negara adalah situs resmi pendaftaran ASN secara nasional sebagai pintu pendaftaran pertama seleksi ASN ke seluruh instansi baik Pusat maupun Daerah dan dikelola oleh Badan Kepegawaian Negara RI sebagai Panitia Seleksi Penerimaan Nasional yang dapat diakses dengan alamat <a href="https://sscasn.bkn.go.id" target="_blank">https://sscasn.bkn.go.id</a></div>
                                </div>
                            </div>
                                                                                <div class="panel bg-white box-shadow-small border-radius-5px mb-3">
                                <div class="panel-heading">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                        href="#collapse5" aria-expanded="false">
                                        <div class="panel-title">
                                            <span
                                                class="alt-font text-extra-dark-gray d-inline-block font-weight-500">Apa yang dimaksud dengan THK II ?</span>
                                            <i class="indicator fas fa-angle-down text-fast-blue icon-extra-small"></i>
                                        </div>
                                    </a>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse"
                                    data-parent="#accordion2">
                                    <div class="panel-body">Individu yang terdaftar dalam database eks tenaga honorer Badan Kepegawaian Negara.</div>
                                </div>
                            </div>
                                                                                <div class="panel bg-white box-shadow-small border-radius-5px mb-3">
                                <div class="panel-heading">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                        href="#collapse6" aria-expanded="false">
                                        <div class="panel-title">
                                            <span
                                                class="alt-font text-extra-dark-gray d-inline-block font-weight-500">Apa yang dimaksud dengan Guru Honorer ?</span>
                                            <i class="indicator fas fa-angle-down text-fast-blue icon-extra-small"></i>
                                        </div>
                                    </a>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse"
                                    data-parent="#accordion2">
                                    <div class="panel-body">Individu yang ditugaskan sebagai guru bukan ASN di satuan Pendidikan yang diselenggarakan oleh Pemerintah Daerah.</div>
                                </div>
                            </div>
                                                                                <div class="panel bg-white box-shadow-small border-radius-5px mb-3">
                                <div class="panel-heading">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                        href="#collapse7" aria-expanded="false">
                                        <div class="panel-title">
                                            <span
                                                class="alt-font text-extra-dark-gray d-inline-block font-weight-500">Apa yang dimaksud dengan Guru Swasta ?</span>
                                            <i class="indicator fas fa-angle-down text-fast-blue icon-extra-small"></i>
                                        </div>
                                    </a>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse"
                                    data-parent="#accordion2">
                                    <div class="panel-body">Individu yang ditugaskan sebagai guru di satuan Pendidikan yang diselenggarakan oleh masyarakat.</div>
                                </div>
                            </div>
                                            </div>
                </div>
                <div class="col-12 text-center mt-md-4">
                    <a href="https://sscasn.bkn.go.id/faq" target="_blank"
                        class="btn btn-fancy btn-medium btn-white btn-round-edge btn-box-shadow">Lihat
                        selengkapnya</a>
                </div>
            </div>
        </div>
    </section>
    <section class="half-section bg-kontak" id="kontak">
        <div class="container">
            <div class="row align-items-center">
                <div
                    class="col-12 col-lg-6 col-md-7 col-sm-6 text-center text-sm-left sm-margin-25px-tb xs-margin-30px-bottom">
                    <h5 style="color: #ffeb3b;text-shadow: 2px 2px 4px #000000;" class="alt-font font-weight-500 margin-10px-bottom">Ada pertanyaan mengenai PPPK?</h5>
                    <a href="https://gurupppk.kemdikbud.go.id/contact" class="btn btn-fancy btn-medium btn-white font-weight-500 btn-round-edge btn-box-shadow pengaduan">Hubungi Kami</a>
                </div>
                <div
                    class="col-12 col-lg-6 col-md-5 col-sm-6 social-icon-style-02 text-center text-sm-right sm-margin-25px-tb xs-no-margin-top">
                    <span class="alt-font text-medium d-block margin-10px-bottom"></span>
                    
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-3" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 id="modal-label-3" class="modal-title">Penjelasan Seleksi Administrasi</h6>
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p style="margin-bottom: 10px"><b>Berkas yang diperlukan untuk seleksi administrasi adalah : </b></p>
                            <ol style="padding-right: 10px">
                                <li>Surat pernyataan yang dapat diunduh pada laman <a target="_blank" href="https://gurupppk.kemdikbud.go.id">https://gurupppk.kemdikbud.go.id</a>;</li>
                                <li>KTP elektronik (e-KTP) asli;</li>
                                <li>Pasfoto;</li>
                                <li>Ijazah;</li>
                                <li>Sertifikat Pendidik bagi yang memiliki;</li>
                                <li>Bagi pendaftar penyandang disabilitas menambahkan;
                                    <ul style="margin-left: 20px;text-align: justify;">
                                        <li>Surat keterangan dari dokter Rumah Sakit Pemerintah/Puskesmas yang menerangkan tentang jenis dan derajat kedisabilitasannya;</li>
                                        <li>Menyampaikan tautan/link video singkat yang menunjukkan kegiatan sehari-hari pelamar dalam menjalankan tugas sebagai pendidik (pelamar mengunggah video singkat tersebut di youtube/googledrive/dropbox/media penyimpanan lainnya).</li>
                                    </ul>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="row mb20">
                        <div class="col-md-12">
                            <p style="text-align: justify;">Verifikasi administrasi dilakukan berdasar pada linieritas Sertifikasi Pendidik dan/atau Kualifikasi Pendidikan. Jika Sertifikasi Pendidik tidak sesuai, dilanjutkan dengan verifikasi berdasar pada linieritas Kualifikasi Pendidikan. Kesesuaian Linieritas dimaksud merujuk pada <a style="color:blue" href="https://gurupppk.kemdikbud.go.id/public/upload/dokumen/SE_Kesesuaian_Kualifikasi_Akademik_dan_Sertifikat_Pendidik.pdf" target="_blank">SE Direktur Jenderal Guru dan Tenaga Kependidikan Kemendikbud No 1460/B.B1/GT.02.01/2021 tanggal 15 Maret 2021.</a></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-b" type="button">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-soal" tabindex="-1" role="modal" aria-labelledby="modal-label-soal" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 id="modal-label-3" class="modal-title">Bobot Soal Ujian Seleksi Kompetensi</h6>
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="img-fluid">
                                <img src="https://gurupppk.kemdikbud.go.id/public/images/bobot_soal.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-b" type="button">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-afirmasi" tabindex="-1" role="modal" aria-labelledby="modal-label-afirmasi" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 id="modal-label-3" class="modal-title">Kebijakan Afirmasi dalam Seleksi Guru PPPK</h6>
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="img-fluid">
                                <img src="https://gurupppk.kemdikbud.go.id/public/images/kebijakan_afirmasi.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-b" type="button">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    </main>
    <!-- start footer -->
    <footer class="bg-extra-dark-gray">
        <div class="footer-top pt-5">
            <div class="container">
                <div class="row">
                    <!-- start footer column -->
                    <div class="col-12 col-md-3">
                        <a href="index.html" class="footer-logo mb-md-4 mb-3 d-block"><img
                                src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png"
                                data-at2x="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png" alt=""
                                class="max-h-inherit"></a>
                        <span
                            class="alt-font font-weight-300 text-medium letter-spacing-minus-1-half d-inline-block w-85 lh-13">

                        </span>
                    </div>
                    <!-- end footer column -->
                    <!-- start footer column -->
                    <div class="col-12 offset-md-1 col-md-5">
                        <span
                            class="alt-font font-weight-500 d-block text-white text-uppercase letter-spacing-2px margin-20px-bottom xs-margin-10px-bottom">
                            Hubungi Kami
                        </span>
                        <ul>
                            <li><a href="#" class="text-white-hover">Kompleks Kementerian Pendidikan dan Kebudayaan,
                                    Gedung D Lantai 16, Jl. Jenderal Sudirman, Senayan, Jakarta 10270</a></li>
                        </ul>
                    </div>
                    <!-- end footer column -->
                    <!-- start footer column -->
                    <div class="col-12 offset-md-1 col-md-2">
                        <span
                            class="alt-font font-weight-500 d-block text-white text-uppercase letter-spacing-2px margin-20px-bottom xs-margin-10px-bottom">
                            Call Center
                        </span>
                        <ul>
                            <li style="font-size:20px"><i class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;<a
                                    href="tel:1 500 997" class="text-white-hover"><b>1 500 997</b></a></li>
                        </ul>
                        <!-- </div> -->
                    </div>
                    <!-- end footer column -->
                </div>
            </div>
        </div>
        <div class="footer-bottom pt-4 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 text-center text-md-right last-paragraph-no-margin">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> Hak Cipta Kemdikbudristek</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->
    <!-- start scroll to top -->
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="feather icon-feather-arrow-up"></i></a>
    <!-- end scroll to top -->
    <!-- javascript -->
    <script src="https://gurupppk.kemdikbud.go.id/public/frontend/js/jquery-3.6.0.min.js"></script>
    
    <script type="text/javascript" src="https://gurupppk.kemdikbud.go.id/public/frontend/js/theme-vendors.min.js"></script>
    <script type="text/javascript" src="https://gurupppk.kemdikbud.go.id/public/frontend/js/main.js"></script>

    
    <script src="https://gurupppk.kemdikbud.go.id/public/frontend/js/sweetalert.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/frontend/js/select2.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/frontend/js/print.min.js"></script>
    <script type="text/javascript" src="https://gurupppk.kemdikbud.go.id/public/frontend/js/simplebar.min.js"></script>

    <script src="https://gurupppk.kemdikbud.go.id/public/frontend/js/imagepreview.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $('.preview').anarchytip();
      $('.previewright').anarchytip();
    </script>

    

    <script src="https://www.google.com/recaptcha/api.js?" async defer></script>

    </body>

</html>
