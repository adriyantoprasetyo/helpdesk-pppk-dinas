
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="WPDArOt15ZGngeokgvbVqPZK9d63Mm6acKgLdW2i">

    <title>PPPK Guru Kemdikbudristek</title>
    <meta name="author" content="Dirjen GTK Kemdikbudristek">
    <meta name="description" content="PPPK Kemdikbudristek">
    <!-- favicon icon -->
    <link rel="shortcut icon" href="https://cdn.tendik.id/public/frontend/images/custom/kemdikbud-sm.png">
    <link rel="apple-touch-icon" href="https://cdn.tendik.id/public/frontend/images/custom/kemdikbud-sm.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://cdn.tendik.id/public/frontend/images/custom/kemdikbud-sm.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://cdn.tendik.id/public/frontend/images/custom/kemdikbud-sm.png">
    <!-- style sheets and font icons  -->
    <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/contact/public/frontend/css/font-icons.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/css/theme-vendors.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/css/style.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/css/simplebar.min.css">

    <!-- revolution slider -->
    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/revolution/css/navigation.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/css/custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.tendik.id/public/frontend/css/custom-2.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-Q8BDM2EQW6"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-Q8BDM2EQW6');
    </script>
</head>

<body data-mobile-nav-style="classic">
    <!-- start header -->
    <header>
        <!-- start navigation -->
        <nav
            class="navbar navbar-expand-lg navbar-dark bg-transparent header-light fixed-top navbar-boxed header-always-fixed-scroll">
            <div class="container-fluid nav-header-container">
                <div class="col-auto col-sm-6 col-lg-2 mr-auto pl-lg-0">
                    <a class="navbar-brand" href="https://gurupppk.kemdikbud.go.id/contact">
                        <img src="https://cdn.tendik.id/public/frontend/images/custom/pppk-light.png"
                            data-at2x="https://cdn.tendik.id/public/frontend/images/custom/pppk-light.png" class="default-logo"
                            alt="">
                        <img src="https://cdn.tendik.id/public/frontend/images/custom/pppk-dark.png"
                            data-at2x="https://cdn.tendik.id/public/frontend/images/custom/pppk-dark.png" class="alt-logo" alt="">
                        <img src="https://cdn.tendik.id/public/frontend/images/custom/pppk-dark.png"
                            data-at2x="https://cdn.tendik.id/public/frontend/images/custom/pppk-dark.png" class="mobile-logo"
                            alt="">
                    </a>
                </div>
                <div class="col-auto col-lg-8 menu-order px-lg-0">
                    <button class="navbar-toggler float-right" type="button" data-toggle="collapse"
                        data-target="#navbarNav" aria-controls="navbarNav" aria-label="Toggle navigation">
                        <span class="navbar-toggler-line"></span>
                        <span class="navbar-toggler-line"></span>
                        <span class="navbar-toggler-line"></span>
                        <span class="navbar-toggler-line"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                        <ul class="navbar-nav alt-font">
                            <li class="nav-item">
                                <a href="https://gurupppk.kemdikbud.go.id" class="nav-link section-link">Beranda</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://gurupppk.kemdikbud.go.id#infopenting" class="nav-link section-link">Pengumuman</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://gurupppk.kemdikbud.go.id#syarat" class="nav-link section-link">Alur Seleksi</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://gurupppk.kemdikbud.go.id#unduhan" class="nav-link section-link">Unduh Dokumen</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://gurupppk.kemdikbud.go.id#faq" class="nav-link section-link">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://gurupppk.kemdikbud.go.id#kontak" class="nav-link section-link">Kontak</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://gurupppk.kemdikbud.go.id/pppk_ppg/" target="_blank" class="nav-link section-link">Cek Data PPG</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-auto col-lg-2 text-right pr-0 font-size-0">
                    
                                    </div>
            </div>
        </nav>

        

            </header>
    <!-- end header -->

    
    <main>
        
    <section class="bg-extra-dark-gray padding-25px-tb page-title-small">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-xl-8 col-lg-6">
                    <h1 class="alt-font text-white font-weight-500 no-margin-bottom text-center text-lg-left"></h1>
                </div>
                <div
                    class="col-12 col-xl-4 col-lg-6 breadcrumb justify-content-center justify-content-lg-end text-small alt-font md-margin-10px-top">
                    <ul class="xs-text-center">
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- end page title -->

        <!-- start section -->
    <section class="wow animate__fadeIn">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                    <div class="row justify-content-center">
                        <div class="col-12 col-xl-6 col-lg-7 text-center margin-4-half-rem-bottom md-margin-3-rem-bottom">
                            <h4 class="alt-font font-weight-500 text-extra-dark-gray">Helpdesk PPPK <br/><b><font color='blue'>Dinas Pendidikan</font></b></h4>
                            <span
                                class="alt-font letter-spacing-minus-1-half text-extra-medium d-block margin-5px-bottom">Lengkapi
                                formulir dibawah ini untuk mengajukan Pertanyaan seputar PPPK Guru Tahun 2022</span>
                        </div>
                        <div class="col-12">
                            <!-- start contact form -->
                            <form id="formpengaduan" action="https://gurupppk.kemdikbud.go.id/contact/dinas/store" method="post">
                                <input type="hidden" name="_token" value="WPDArOt15ZGngeokgvbVqPZK9d63Mm6acKgLdW2i"> <input type="hidden" name="_method" value="post">                                <div class="row row-cols-1 row-cols-md-2">
                                    <div class="col margin-4-rem-bottom sm-margin-25px-bottom">
                                        <input class="medium-input bg-white margin-25px-bottom required" type="text"
                                            name="nama" placeholder="Nama Lengkap" value=""
                                            autocomplete="off">
                                        
                                        <input class="medium-input bg-white margin-25px-bottom required" type="email"
                                            name="email" placeholder="Email" value=""
                                            autocomplete="off">
                                        
                                        <input class="medium-input bg-white margin-25px-bottom required" type="number"
                                            name="no_hp" placeholder="No HP" value=""
                                            autocomplete="off">
                                        
                                        <select name="asal_provinsi" id="asal_provinsi"
                                            class="medium-select medium-input select bg-white margin-25px-bottom required"
                                            placeholder="Asal Provinsi">
                                            <option value="010000">Prov. D.K.I. Jakarta</option>
                                        </select>
                                        
                                        <select name="asal_kabupaten_kota" id="asal_kabupaten_kota"
                                            class="medium-select select bg-white margin-25px-bottom required"
                                            placeholder="Asal Kabupaten / Kota">
                                            <option value="016100">Kota Jakarta Utara</option>
                                        </select>
                                                                                

                                    </div>
                                    <div class="col margin-4-rem-bottom sm-margin-10px-bottom">
                                        <input class="medium-input bg-white margin-25px-bottom required" type="text"
                                            name="judul" placeholder="Judul Pengaduan" autocomplete="off">
                                            <textarea class="medium-textarea h-200px bg-white required" name="isi"
                                            placeholder="Isi pengaduan" autocomplete="off"></textarea>
                                    
                                    <table style="width:100%">
                                        <tr>
                                            <td style="width:30%;font-size:22px;vertical-align:middle;text-align:center">10 + 12</td>
                                            <td style="width:70%"><input class="medium-input bg-white required" type="number"
                                            name="captcha" placeholder="Masukkan Hasil Angka disamping" value=""
                                            autocomplete="off" style="display:inline"></td>
                                        </tr>
                                    </table>                                        
                                        
                                    </div>
                                    <div class="col text-left sm-margin-30px-bottom">
        
                                    </div>
                                    <div class="col text-center text-md-right">
                                        <input type="hidden" name="redirect" value="">
                                        <button class="btn btn-medium btn-gradient-light-purple-light-orange mb-0 submit"
                                            type="submit">Kirim Pengaduan</button>
                                    </div>
                                </div>
                                <div class="form-results d-none"></div>
                            </form>
                            <!-- end contact form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </main>
    <!-- start footer -->
    <footer class="bg-extra-dark-gray">
        <div class="footer-top pt-5">
            <div class="container">
                <div class="row">
                    <!-- start footer column -->
                    <div class="col-12 col-md-3">
                        <a href="index.html" class="footer-logo mb-md-4 mb-3 d-block"><img
                                src="https://cdn.tendik.id/public/frontend/images/custom/pppk-light.png"
                                data-at2x="https://cdn.tendik.id/public/frontend/images/custom/pppk-light.png" alt=""
                                class="max-h-inherit"></a>
                        <span
                            class="alt-font font-weight-300 text-medium letter-spacing-minus-1-half d-inline-block w-85 lh-13">

                        </span>
                    </div>
                    <!-- end footer column -->
                    <!-- start footer column -->
                    <div class="col-12 offset-md-1 col-md-5">
                        <span
                            class="alt-font font-weight-500 d-block text-white text-uppercase letter-spacing-2px margin-20px-bottom xs-margin-10px-bottom">
                            Hubungi Kami
                        </span>
                        <ul>
                            <li><a href="#" class="text-white-hover">Kompleks Kementerian Pendidikan dan Kebudayaan,
                                    Gedung D Lantai 16, Jl. Jenderal Sudirman, Senayan, Jakarta 10270</a></li>
                        </ul>
                    </div>
                    <!-- end footer column -->
                    <!-- start footer column -->
                    <div class="col-12 offset-md-1 col-md-2">
                        <span
                            class="alt-font font-weight-500 d-block text-white text-uppercase letter-spacing-2px margin-20px-bottom xs-margin-10px-bottom">
                            Call Center
                        </span>
                        <ul>
                            <li style="font-size:20px"><i class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;<a
                                    href="tel:1 500 997" class="text-white-hover"><b>1 500 997</b></a></li>
                        </ul>
                        <!-- </div> -->
                    </div>
                    <!-- end footer column -->
                </div>
            </div>
        </div>
        <div class="footer-bottom pt-4 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 text-center text-md-right last-paragraph-no-margin">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> Hak Cipta Kemdikbudristek</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->
    <!-- start scroll to top -->
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="feather icon-feather-arrow-up"></i></a>
    <!-- end scroll to top -->
    <!-- javascript -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="https://cdn.tendik.id/public/frontend/js/theme-vendors.min.js"></script>
    <script type="text/javascript" src="https://cdn.tendik.id/public/frontend/js/main.js"></script>

    <!-- revolution js files -->
    <script type="text/javascript" src="https://cdn.tendik.id/public/frontend/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="https://cdn.tendik.id/public/frontend/revolution/js/jquery.themepunch.revolution.min.js">
    </script>

    <script type="text/javascript"
        src="https://cdn.tendik.id/public/frontend/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript"
        src="https://cdn.tendik.id/public/frontend/revolution/js/extensions/revolution.extension.layeranimation.min.js">
    </script>
    <script type="text/javascript"
        src="https://cdn.tendik.id/public/frontend/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
    <script type="text/javascript" src="https://cdn.tendik.id/public/frontend/js/simplebar.min.js"></script>

    <script type="text/javascript">
        var revapi263,
            tpj;
        (function() {
            if (tpj === undefined) {
                tpj = jQuery;
                if ("off" == "on")
                    tpj.noConflict();
            }
            if (!/loaded|interactive|complete/.test(document.readyState))
                document.addEventListener("DOMContentLoaded", onLoad);
            else
                onLoad();

            function onLoad() {
                if (tpj("#rev_slider_26_1").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_26_1");
                } else {
                    var revOffset = tpj(window).width() <= 991 ? '73px' : '';
                    revapi263 = tpj("#rev_slider_26_1").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "revolution/js/",
                        sliderLayout: "fullscreen",
                        dottedOverlay: "none",
                        delay: 4500,
                        navigation: {
                            keyboardNavigation: "on",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "off",
                            touch: {
                                touchenabled: "on",
                                touchOnDesktop: "on",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            },
                            arrows: {
                                enable: true,
                                style: 'ares',
                                tmp: '',
                                rtl: false,
                                hide_onleave: false,
                                hide_onmobile: true,
                                hide_under: 767,
                                hide_over: 9999,
                                hide_delay: 0,
                                hide_delay_mobile: 0,

                                left: {
                                    container: 'slider',
                                    h_align: 'left',
                                    v_align: 'center',
                                    h_offset: 60,
                                    v_offset: 0
                                },

                                right: {
                                    container: 'slider',
                                    h_align: 'right',
                                    v_align: 'center',
                                    h_offset: 60,
                                    v_offset: 0
                                }
                            },
                            bullets: {
                                enable: true,
                                style: 'zeus',
                                direction: 'horizontal',
                                rtl: false,

                                container: 'slider',
                                h_align: 'center',
                                v_align: 'bottom',
                                h_offset: 0,
                                v_offset: 30,
                                space: 7,

                                hide_onleave: false,
                                hide_onmobile: false,
                                hide_under: 0,
                                hide_over: 767,
                                hide_delay: 200,
                                hide_delay_mobile: 1200
                            },
                        },
                        responsiveLevels: [1240, 1025, 778, 480],
                        visibilityLevels: [1920, 1500, 1025, 768],
                        gridwidth: [1200, 991, 778, 480],
                        gridheight: [1025, 1366, 1025, 868],
                        lazyType: "none",
                        shadow: 0,
                        spinner: "spinner4",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "on",
                        fullScreenAutoWidth: "on",
                        fullScreenAlignForce: "off",
                        fullScreenOffsetContainer: "",
                        fullScreenOffset: revOffset,
                        disableProgressBar: "on",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLimit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }; /* END OF revapi call */
            }; /* END OF ON LOAD FUNCTION */
        }()); /* END OF WRAPPING FUNCTION */
    </script>
    <script src="https://www.google.com/recaptcha/api.js?" async defer></script>

    
</body>

</html>