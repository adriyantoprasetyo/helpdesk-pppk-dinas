<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>PPPK Guru Kemdikbudristek</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Seleksi Penerimaan PPPK Guru Kemdikbudristek Tahun 2021" name="description" />
    <meta content="Ditjen GTK Kemdikbudristek" name="author" />
    <link rel="shortcut icon" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">

    <!-- Bootstrap Css -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/assets/libs/sweetalert2/sweetalert2.min.css">
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet"
    type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet"
    type="text/css" />

</head>


<body data-sidebar="dark">
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- BEGIN: Header-->
        <header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="#" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png" alt="" height="17">
                    </span>
                </a>

                <a href="#" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png" alt="" height="60"
                            width="200">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <!-- App Search-->
            <form class="app-search d-none d-lg-block">
                <div class="position-relative">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="bx bx-search-alt"></span>
                </div>
            </form>

        </div>

        <div class="d-flex">

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user"
                        src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png" alt="Header Avatar">
                    <span class="d-none d-xl-inline-block ml-1" key="t-henry"><?php echo $jenisakun ?></span>
                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <!-- <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i> <span key="t-profile">Profile</span></a> -->
                    <a class="dropdown-item text-danger" href="https://gurupppk.kemdikbud.go.id/logout"><i
                            class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> <span
                            key="t-logout">Logout</span></a>
                </div>
            </div>

        </div>
    </div>
</header>
        <!-- END: Header-->


        <!-- BEGIN: Sidebar-->
        <!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                                    <li class="menu-title" key="t-menu">Menu</li>
                    <li>
                        <a href="https://gurupppk.kemdikbud.go.id/pindah/dashboard" class="waves-effect">
                            <i class="bx bx-home-circle"></i>
                            
                            <span key="t-dashboards">Dashboard</span>
                        </a>
                    </li>
                    <li class="menu-title" key="t-informasi">Informasi</li>
                    <?php
                    if ($jenisakun=="admin")
                    {
                        ?>
                    <li>
                        <a href="https://gurupppk.kemdikbud.go.id/layanan/plotting" class="waves-effect">
                            <i class="bx bx-stats"></i>
                            <span key="t-chat">Plotting</span>
                        </a>
                    </li>

                    <li>
                        <a href="https://gurupppk.kemdikbud.go.id/pindah" class="waves-effect">
                            <i class="bx bx-chat"></i>
                            <span key="t-chat">Pengaduan</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://gurupppk.kemdikbud.go.id/admin/faq" class="waves-effect">
                            <i class="bx bx-message"></i>
                            <span key="t-chat">FAQ</span>
                        </a>
                    </li>

                    <li>
                        <a href="https://gurupppk.kemdikbud.go.id/admin/dokumen" class="waves-effect">
                            <i class="bx bxs-file-blank"></i>
                            <span key="t-chat">Dokumen</span>
                        </a>
                    </li>

                    <li>
                        <a href="https://gurupppk.kemdikbud.go.id/admin/operator" class="waves-effect">
                            <i class="bx bx-user"></i>
                            <span key="t-chat">Operator</span>
                        </a>
                    </li>
                    <?php
                    }
                    else
                    {
                        ?>
                    <li>
                        <a href="https://gurupppk.kemdikbud.go.id/pindah" class="waves-effect">
                            <i class="bx bx-chat"></i>
                            <span key="t-chat">Pengaduan</span>
                        </a>
                    </li>
                        <?php
                    }
                    ?>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End
        <!-- END: Sidebar-->


        <!-- BEGIN: Content-->
            <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Dashboard</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Informasi</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row">
                    <div class="col-md-3">
                        <div class="card mini-stats-wid">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-medium">Belum Terjawab</p>
                                        <h4 class="mb-0"><?php echo number_format($stat1,0) ?></h4>
                                    </div>

                                    <div class="mini-stat-icon avatar-sm rounded-circle bg-danger align-self-center">
                                        <span class="avatar-title  rounded-circle bg-danger">
                                            <i class="bx bx-copy-alt font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card mini-stats-wid">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-medium">Aktif - Sudah Terjawab</p>
                                        <h4 class="mb-0"><?php echo number_format($stat2,0) ?></h4>
                                    </div>

                                    <div class="avatar-sm rounded-circle bg-primary align-self-center mini-stat-icon">
                                        <span class="avatar-title rounded-circle bg-primary">
                                            <i class="bx bx-copy-alt font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card mini-stats-wid">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-medium">Aktif - Belum Terjawab</p>
                                        <h4 class="mb-0"><?php echo number_format($stat3,0) ?></h4>
                                    </div>

                                    <div class="avatar-sm rounded-circle bg-warning align-self-center mini-stat-icon">
                                        <span class="avatar-title rounded-circle bg-warning">
                                            <i class="bx bx-copy-alt font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card mini-stats-wid">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-medium">Selesai</p>
                                        <h4 class="mb-0"><?php echo number_format($stat4,0) ?></h4>
                                    </div>

                                    <div class="avatar-sm rounded-circle bg-success align-self-center mini-stat-icon">
                                        <span class="avatar-title rounded-circle bg-success">
                                            <i class="bx bx-copy-alt font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Report per Propinsi</h4>
                                <table id="datatable" class="table table-bordered dt-responsive"
                                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="vertical-align: middle" rowspan="2">No</th>
                                            <th class="text-center" style="vertical-align: middle" rowspan="2">Nama Propinsi</th>
                                            <th class="text-center" style="vertical-align: middle" rowspan="2">Jumlah Total</th>
                                            <th class="text-center" style="vertical-align: middle" colspan="4">Progress Detail</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center" style="vertical-align: middle">Belum Terjawab</th>
                                            <th class="text-center" style="vertical-align: middle">Aktif - Sudah Terjawab</th>
                                            <th class="text-center" style="vertical-align: middle">Aktif - Belum Terjawab</th>
                                            <th class="text-center" style="vertical-align: middle">Selesai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php
                                     $no=0;
                                     foreach ($queryasli->result_array() as $r)
                                     {
                                        if (strlen($r['asal_provinsi'])==5)
                                        {
                                            $prop="0".$r['asal_provinsi'];
                                        }
                                        else
                                        {
                                            $prop=$r['asal_provinsi'];
                                        }

                                        $cekprop=$this->db->where('kode_wilayah',$prop)->get('master_propinsi')->row_array()['nama'];
                                        $no++;
                                        ?>
                                        <tr>
                                            <td style="text-align: center"><?php echo $no ?></td>
                                            <td><?php echo $cekprop?></td>
                                            <td style="text-align: center"><?php echo $r['jum_0'] ?></td>
                                            <td style="text-align: center"><?php echo $r['jum_1'] ?></td>
                                            <td style="text-align: center"><?php echo $r['jum_2'] ?></td>
                                            <td style="text-align: center"><?php echo $r['jum_3'] ?></td>
                                            <td style="text-align: center"><?php echo $r['jum_4'] ?></td>
                                        </tr>
                                        <?php
                                     }
                                     ?>
                                    </tbody>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- END: Content-->

    </div>
    <!-- END layout-wrapper -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- JAVASCRIPT -->
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/jquery/jquery.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/metismenu/metisMenu.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/simplebar/simplebar.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/node-waves/waves.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- App js -->
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/js/app.js"></script>

    <!-- BEGIN: Page JS-->
        <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/sweetalert2/sweetalert2.min.js"></script>
    <script>
        function show_swal(tipe, title, message) {
            Swal.fire(
                title, message, tipe
            )
        }
    </script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script>
        let default_lang = {
            paginate: {
                next: 'Selanjutnya',
                previous: 'Sebelumnya'
            },
            lengthMenu: "Menampilkan _MENU_ baris",
            search: 'Cari:',
            info: 'Menampilkan _START_ ke _END_ dari _TOTAL_ baris',
            infoEmpty: 'Kosong',
            emptyTable: 'Tidak ada data yang dapat ditampilkan',
            infoFiltered: '(tersaring dari _MAX_ baris)'
        };
    </script>
    <script>
        $(document).ready(function () {
            var oTable = $('#datatable').DataTable();
    </script>
    <!-- END: Page JS-->

</body>

</html>