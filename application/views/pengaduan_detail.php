<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>PPPK Guru Kemdikbudristek</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Seleksi Penerimaan PPPK Guru Kemdikbudristek Tahun 2021" name="description" />
    <meta content="Ditjen GTK Kemdikbudristek" name="author" />
    <link rel="shortcut icon" href="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png">

    <!-- Bootstrap Css -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="https://gurupppk.kemdikbud.go.id/public/assets/libs/sweetalert2/sweetalert2.min.css">
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet"
    type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="https://gurupppk.kemdikbud.go.id/public/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet"
    type="text/css" />

    <style>
        .swal2-input{
            margin-left: 5px !important;
            margin-right: 5px !important;
            width: 100% !important;
        }
    </style>

</head>


<body data-sidebar="dark">
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- BEGIN: Header-->
        <header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="#" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-dark.png" alt="" height="17">
                    </span>
                </a>

                <a href="#" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/pppk-light.png" alt="" height="60"
                            width="200">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <!-- App Search-->
            <form class="app-search d-none d-lg-block">
                <div class="position-relative">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="bx bx-search-alt"></span>
                </div>
            </form>

        </div>

        <div class="d-flex">

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user"
                        src="https://gurupppk.kemdikbud.go.id/public/frontend/images/custom/kemdikbud-sm.png" alt="Header Avatar">
                    <span class="d-none d-xl-inline-block ml-1" key="t-henry"><?php echo $jenisakun ?></span>
                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <!-- <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i> <span key="t-profile">Profile</span></a> -->
                    <a class="dropdown-item text-danger" href="<?php echo base_url('index.php/login/logout')?>"><i
                            class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> <span
                            key="t-logout">Logout</span></a>
                </div>
            </div>

        </div>
    </div>
</header>
        <!-- END: Header-->


        <!-- BEGIN: Sidebar-->
        <!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                                    <li class="menu-title" key="t-menu">Menu</li>
                    <li>
                        <a href="<?php echo base_url('index.php/dashboard/dinas/'.$this->session->userdata('jenisakun').'/'.$this->session->userdata('userid').'')?>" class="waves-effect">
                            <i class="bx bx-home-circle"></i>
                            
                            <span key="t-dashboards">Dashboard</span>
                        </a>
                    </li>
                    <li class="menu-title" key="t-informasi">Informasi</li>
                    <?php
                    if ($this->session->userdata('jenisakun')=="admin")
                    {
                        ?>
                        <li>
                            <a href="<?php echo base_url('index.php/plotting')?>" class="waves-effect">
                                <i class="bx bx-stats"></i>
                                <span key="t-chat">Plotting</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('index.php/pengaduan/index/'.$this->session->userdata('jenisakun').'/'.$this->session->userdata('userid').'')?>" class="waves-effect">
                                <i class="bx bx-chat"></i>
                                <span key="t-chat">Pengaduan</span>
                            </a>
                        </li>
                    <?php
                    }
                    else
                    {
                        ?>
                    <li>
                        <a href="<?php echo base_url('index.php/pengaduan/index/'.$this->session->userdata('jenisakun').'/'.$this->session->userdata('userid').'')?>" class="waves-effect">
                            <i class="bx bx-chat"></i>
                            <span key="t-chat">Pengaduan</span>
                        </a>
                    </li>
                        <?php
                    }
                    ?>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End
        <!-- END: Sidebar-->


        <!-- BEGIN: Content-->
            <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Pengaduan</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Informasi</a></li>
                                    <li class="breadcrumb-item active">Pengaduan</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('msg')!="")
                                {
                                    ?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <?php echo $this->session->flashdata('msg') ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <?php
                                }
                                ?>
                                <h4 class="card-title mb-4">Judul: <?php echo $pengaduan['judul']?>
                                 <?php
                                 if ($pengaduan['status']=='1')
                                 {
                                    ?>
                                 <form action="<?php echo base_url('index.php/pengaduan/close_chat/'.$pengaduan['tiket'].'')?>" class="form-inline d-inline" style="float:right;" method="post">
                                    <input type="hidden" name="_token" value="mvDxz2NIK2M0ektfl92Krg4YS066dZN8fcVPyTl1"> <input type="hidden" name="_method" value="post"><button class="btn btn-danger d-inline" id="selesai_pengaduan"><i class="fas fa-ban"></i> Selesai pengaduan</button>
                                 </form>
                                    <?php
                                }
                                ?>
                                 <a href="<?php echo base_url('index.php/pengaduan/index/'.$this->session->userdata('jenisakun').'/'.$this->session->userdata('userid').'')?>" style="float:right;margin-right: 10px" class="btn btn-warning d-inline">Kembali</a>
                                </h4>
                                <div class="chat-conversation p-3">
                                    <ul id="chat_section" class="list-unstyled mb-0 overflow-auto" style="max-height: 486px;">
                                        <?php
                                        foreach ($riwayat as $rr)
                                        {
                                            if ($rr['who']=='0')
                                            {
                                                $style="";
                                                $nama=$pengaduan['nama'];
                                                $aksi="";
                                            }
                                            else if ($rr['who']=='1')
                                            {
                                                $style="right";
                                                $nama=$this->db->where('id',$rr['user_id'])->get('users')->row_array()['fullname'];

                                                if ($pengaduan['status']=='1')
                                                {
                                                    $aksi='- <a href="#" onclick="ubahChat(event, '.$rr['id'].')" class="ubah_chat font-weight-bold text-primary">Ubah</a> - <a href="#" onclick="hapusChat(event, '.$rr['id'].')" class="font-weight-bold text-danger">Hapus</a>';
                                                }
                                                else
                                                {
                                                    $aksi='';
                                                }
                                            }
                                            ?>
                                            <li class="<?php echo $style ?>">
                                                <div class="conversation-list">
                                                    <div class="ctext-wrap">
                                                        <div class="conversation-name"><?php echo $nama ?></div>
                                                        <p>
                                                            <?php echo $rr['isi']?>
                                                        </p>
                                                        <p class="chat-time mb-0"><i class="bx bx-time-five align-middle mr-1"></i>
                                                            <?php echo $rr['created_at']?> <?php echo $aksi ?></p>
                                                    </div>

                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <div class="p-3 chat-input-section">
                                    <div class="row">
                                        <?php
                                        if ($pengaduan['status']=='1')
                                        {
                                            ?>
                                            <form action="<?php echo base_url('index.php/pengaduan/send/'.$pengaduan['tiket'].'')?>" id="form_send"
                                            method="post" class="form-inline" style="width:100%">
                                            <input type="hidden" name="_method" value="post">                                            
                                            <div class="col">
                                                <div class="position-relative">
                                                    <input type="text" autocomplete="off" style="width: 100%" name="isi" required class="form-control isi chat-input" placeholder="Tuliskan Pesan Anda...">                                               
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="kirim_pesan" class="btn btn-primary btn-rounded chat-send w-md waves-effect waves-light"><span class="d-none d-sm-inline-block mr-2">Send</span><i class="mdi mdi-send"></i></button><button id="loader" class="btn btn-primary btn-rounded chat-send w-md waves-effect waves-light" type="button" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...</button>
                                            </div>
                                            </form>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                        <form action="http://118.98.166.135/webpppk/admin/pengaduan/send/1" id="form_send"
                                            method="post" class="form-inline" style="width:100%">
                                            <input type="hidden" name="_token" value="UxH9AxqOMmoVYDYBZYkVHKTt5LYCfwZebIyX8tfQ"> 
                                            <input type="hidden" name="_method" value="post">                                            
                                            <div class="col">
                                                <div class="position-relative">
                                                    <input type="text" autocomplete="off" style="width: 100%" name="isi"  disabled class="form-control isi chat-input" placeholder="Pengaduan telah ditutup">    
                                                </div>
                                            </div>
                                            <div class="col-auto"></div>
                                        </form>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- END: Content-->

    </div>
    <!-- END layout-wrapper -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- JAVASCRIPT -->
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/jquery/jquery.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/metismenu/metisMenu.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/simplebar/simplebar.min.js"></script>
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/node-waves/waves.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- App js -->zzzz
    <script src="https://gurupppk.kemdikbud.go.id/public/assets/js/app.js"></script>

    <!-- BEGIN: Page JS-->
        <script src="https://gurupppk.kemdikbud.go.id/public/assets/libs/sweetalert2/sweetalert2.min.js"></script>
    <script>
        function show_swal(tipe, title, message) {
            Swal.fire(
                title, message, tipe
            )
        }
    </script>
    <script>
        $(document).ready(function() {
            $('#loader').hide();
            $('#form_send').submit(function(event) {
                event.preventDefault()
                let formData = new FormData(this);
                formData.append('_token', 'BiD2WTmqE02m5igzsWfVpx02nRzvmkZaSZz8ANgX')
                $.ajax({
                    url: $(this).attr('action'),
                    type: 'post',
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        $('#loader').show()
                        $('#kirim_pesan').hide()
                    },
                    success: function(data) {
                        $('#kirim_pesan').show()
                        $('#loader').hide()
                        Swal.fire({
                            title: 'Berhasil!',
                            text: `${data.message}`,
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        // setTimeout(() => {
                            location.reload();
                        // }, 1000);
                        // initChat();

                    },
                    error: function(error) {
                        console.log(error);
                    }
                })
            })

            $('#selesai_pengaduan').click(function(event) {
                event.preventDefault()
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Anda tidak bisa mengembalikannya!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, tutup pengaduan!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            title: 'Berhasil!',
                            text: 'Pengaduan berhasil ditutup',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        setTimeout(() => {
                            $(this).parent().submit()
                        }, 1000);
                    }
                })
            })

            ubahChat = async (event, id) => {
                event.preventDefault()
                let route_edit = `<?php echo base_url('index.php/pengaduan/ubah_chat')?>/:id`
                route_edit = route_edit.replace(':id', id)

                const inputValue = fetch(route_edit)
                    .then(response => response.json())
                    .then(data => data.message)

                Swal.fire({
                    title: 'Edit Chat',
                    input: 'text',
                    inputLabel: '',
                    inputValue: inputValue,
                    showCancelButton: true,
                    inputValidator: (value) => {
                        if (!value) {
                            return 'You need to write something!'
                        }
                    },

                }).then(result => {
                    // console.log(result)
                    if (result.isConfirmed) {
                        let route_update = `<?php echo base_url('index.php/pengaduan/update_chat')?>/:id`
                        let formData = new FormData();
                        formData.append('id', id)
                        formData.append('isi', result.value)
                        formData.append('_method', 'POST')
                        route_update = route_update.replace(':id', id)
                        $.ajax({
                            url: route_update,
                            type: 'post',
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function(success) {
                                console.log(success)
                                Swal.fire(
                                    'Berhasil!',
                                    'Berhasil diubah!',
                                    'success'
                                )
                                location.reload()
                            },
                            error: function(error) {
                                console.error(error);
                            }
                        })
                    }
                })
            }

            hapusChat = async (event, id) => {
                event.preventDefault()
                let route_delete = `<?php echo base_url('index.php/pengaduan/delete_chat')?>/:id`
                route_delete = route_delete.replace(':id', id)

                Swal.fire({
                    title: 'Apakah anda yakin untuk menghapus?',
                    text: "Anda tidak bisa mengembalikannya lagi!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Hapus!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        let formData = new FormData()
                        formData.append('id', id)
                        formData.append('_token', 'BiD2WTmqE02m5igzsWfVpx02nRzvmkZaSZz8ANgX')
                        formData.append('_method', 'POST')
                        $.ajax({
                            url: route_delete,
                            type: 'post',
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function(success) {
                                console.log(success)
                                Swal.fire(
                                    'Berhasil!',
                                    `Chat Berhasil Dihapus!`,
                                    'success'
                                )
                                location.reload()
                            },
                            error: function(error) {
                                console.error(error)
                            }
                        })
                    }
                })
            }
        })
    </script>

</body>

</html>